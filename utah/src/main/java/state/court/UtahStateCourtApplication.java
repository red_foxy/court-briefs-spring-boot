package state.court;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;
import state.court.service.CourtService;

/**
 * Hello world!
 */
@SpringBootApplication
public class UtahStateCourtApplication implements CommandLineRunner {

    private final CourtService service;

    @Autowired
    public UtahStateCourtApplication(@Qualifier("utahCourtServiceImpl") CourtService service) {
        this.service = service;
    }

    public static void main( String[] args )
    {
        SpringApplication.run(UtahStateCourtApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
//        service.scrapeCourt();
    }
}
