package state.court.utah.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import state.court.service.CourtService;
import state.court.utah.scraper.UtahCourtScraper;

import java.io.IOException;

/**
 * @author foxy
 * @since 26.12.18.
 */
@Service
public class UtahCourtServiceImpl implements CourtService {

    private final UtahCourtScraper utahScraper;

    @Autowired
    public UtahCourtServiceImpl(UtahCourtScraper utahScraper) {
        this.utahScraper = utahScraper;
    }

    @Override
    @Transactional
    public void scrapeCourt() throws IOException {
        utahScraper.parsePage();
    }
}
