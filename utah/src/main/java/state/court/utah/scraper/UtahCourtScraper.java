package state.court.utah.scraper;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import state.court.driver.WebDriverWrapper;
import state.court.entity.FileNames;
import state.court.entity.ScraperInfo;
import state.court.io.FileOutputImpl;
import state.court.model.MetaData;
import state.court.repository.FileNameRepository;
import state.court.repository.ScraperRepository;
import state.court.utah.parser.UtahCourtParser;
import state.court.utils.ParserUtils;
import state.court.utils.ScraperUtils;
import state.court.utils.ZipUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;
import static state.court.constant.Constants.LINK_ATTR_SELECTOR;
import static state.court.utah.constant.UtahCourtConstants.*;

/**
 * @author foxy
 * @since 26.12.18.
 */
@Component
public class UtahCourtScraper {

    private final Logger logger = getLogger(UtahCourtScraper.class);

    private final ScraperRepository scraperRepository;
    private final FileNameRepository fileNameRepository;
    private final UtahCourtParser utahParser;

    @Autowired
    public UtahCourtScraper(ScraperRepository scraperRepository,
                            FileNameRepository fileNameRepository,
                            UtahCourtParser utahParser) {
        this.scraperRepository = scraperRepository;
        this.fileNameRepository = fileNameRepository;
        this.utahParser = utahParser;
    }

    public void parsePage() throws IOException {
        ScraperInfo scraperInfo = scraperRepository.save(ParserUtils.saveScraperInfo(SUB_DIR_NAME));
        final List<String> briefUrlList = new ArrayList<>();
        try (final WebDriverWrapper webDriver = new WebDriverWrapper()) {
            webDriver.get(MAIN_URL);
            while (true) {
                webDriver.delay(3000, 3500);
                ScraperUtils.scrapeBriefLinkList(briefUrlList, webDriver, BRIEF_LINK_SELECTOR, LINK_ATTR_SELECTOR);
                By nextPageCssSelector = By.cssSelector(NEXT_PAGE_SELECTOR);
                if (webDriver.exists(nextPageCssSelector)) {
                    clickButton(webDriver, nextPageCssSelector);
                } else {
                    break;
                }
            }
        }
        parsePageByUrl(briefUrlList, scraperInfo);
        ZipUtils.archiveFiles(SUB_DIR_NAME, SUB_DIR_NAME_UPDATE);
    }

    private void parsePageByUrl(List<String> briefUrlList, ScraperInfo scraperInfo) throws IOException {
        List<String> fileNames = new ArrayList<>();
        try (final WebDriverWrapper webDriver = new WebDriverWrapper()) {
            for (String url : briefUrlList) {
                webDriver.get(url);
                webDriver.delay(3000, 3500);
                String pageSource = webDriver.getPageSource();
                final List<MetaData> metaDataList = utahParser.parsePage(pageSource);
                for (MetaData metaData : metaDataList) {
                    final List<String> downloadPDFList = downloadPDF(metaData, scraperInfo);
                    if (!downloadPDFList.isEmpty())
                        fileNames.addAll(downloadPDFList);
                }
                logger.info("MetaDataList size {}", metaDataList.size());
                ParserUtils.setFileNameToScraperInfo(fileNames, scraperInfo);
                scraperRepository.save(scraperInfo);
            }
        }
    }

    private void clickButton(WebDriverWrapper webDriver, By nextPageCssSelector) {
        WebElement nextPageButton = webDriver.getWebDriver().findElement(nextPageCssSelector);
        logger.info("clickButton");
        if (webDriver.getWebDriver() instanceof JavascriptExecutor) {
            logger.info("webDriver.getWebDriver() instanceof JavascriptExecutor");
            JavascriptExecutor jsExecutor = (JavascriptExecutor) webDriver.getWebDriver();
            jsExecutor.executeScript(
                    "arguments[0].parentNode.removeChild(arguments[0])", webDriver.getWebDriver().findElement(By.xpath("/html/body/div[1]")));
        }
        nextPageButton.click();
    }

    private List<String> downloadPDF(MetaData metaData, ScraperInfo scraperInfo) throws IOException {
        FileOutputImpl output = new FileOutputImpl(SUB_DIR_NAME);
        List<String> existedFiles = getExistFiles(scraperInfo, output);
        return output.downloadPDF(metaData, SUB_DIR_NAME, existedFiles, scraperInfo);
    }

    private List<String> getExistFiles(ScraperInfo scraperInfo, FileOutputImpl output) {
        List<FileNames> fileNames = fileNameRepository.findAllByScraperInfoScraperName(scraperInfo.getScraperName());
        return output.getExistFiles(fileNames);
    }
}
