package state.court.utah.constant;

/**
 * @author foxy
 * @since 22.04.19.
 */
public class UtahCourtConstants {

    public static final String SUB_DIR_NAME = "utah";
    public static final String SUB_DIR_NAME_UPDATE = "/data/" + SUB_DIR_NAME;
    public static final String MAIN_URL = "https://digitalcommons.law.byu.edu/utah_briefs_collection/index.html";
    public static final String BRIEF_LINK_SELECTOR = "#series-home > p.article-listing > a";
    public static final String NEXT_PAGE_SELECTOR = "#series-home > div.adjacent-pagination > ul > li > a.next";


    private UtahCourtConstants() {
    }
}
