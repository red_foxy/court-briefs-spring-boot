package state.court.utah.parser;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;
import state.court.model.MetaData;
import state.court.utils.ParserUtils;
import state.court.utils.ScraperUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static state.court.constant.Constants.DATE_PATTERN;

/**
 * @author foxy
 * @since 22.04.19.
 */
@Slf4j
@Component
public class UtahCourtParser {

    public List<MetaData> parsePage(String page) {
        List<MetaData> metaDataList = new ArrayList<>();
        Document mainDocument = Jsoup.parse(page);
        if (mainDocument != null) {
            MetaData metaData = new MetaData();
            String caseNumber = parseCaseNumber(mainDocument);
            String caseName = parseCaseName(mainDocument);
            metaData.setCaseName(caseName);
            metaData.setDocketOrCaseNumber(caseNumber);
            metaData.setFileName(ParserUtils.getFilename(caseNumber, caseName, 0));
            metaData.setJurisdiction(parseJurisdiction(mainDocument));
            metaData.setDescription(parseDescription(mainDocument));
            metaData.setBriefField(parseFieldBrief(mainDocument));
            metaData.setAppellateBriefUrl(parseAppellateBrief(mainDocument));
            metaData.setRetrieved(LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_PATTERN)));
            metaDataList.add(metaData);
        }
        return metaDataList;
    }

    private String parseCaseName(Document mainDocument) {
        String dirtyCaseName = mainDocument.selectFirst("#title > p").text();
        return StringUtils.substringBefore(dirtyCaseName, ":").trim();
    }

    private String parseCaseNumber(Document mainDocument) {
        return mainDocument.selectFirst("#identifier > p").text().trim();
    }

    private String parseJurisdiction(Document mainDocument) {
        String jurisdiction = mainDocument.selectFirst("#authors > p > a > strong").text().trim();
        if (StringUtils.isBlank(jurisdiction)) {
            return "Utah Court of Appeals";
        }
        return jurisdiction;
    }

    private String parseDescription(Document mainDocument) {
        return mainDocument.selectFirst("#document_type > p").text().trim();
    }

    private String parseFieldBrief(Document mainDocument) {
        String year = mainDocument.selectFirst("#publication_date > p").text().trim();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.valueOf(year));
        cal.set(Calendar.DAY_OF_YEAR, 1);
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        return dateFormat.format(date);
    }

    private String parseAppellateBrief(Document mainDocument) {
        return ScraperUtils.scrapeBriefLink(mainDocument, "#pdf", "href");
    }
}
