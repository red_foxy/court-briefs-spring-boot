package state.court;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple TexasStateCourtApplication.
 */
@Slf4j
public class TexasStateCourtApplicationTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void selectDescriptionTest() throws IOException {
        Document document = Jsoup.parse(new File("/home/foxy/Projects/state-court/texas/src/test/resources/texas.html"), "UTF-8");
        Elements appellateBriefRowElementList = document.select("#ctl00_ContentPlaceHolder1_grdBriefs_ctl00 tbody tr");
        for (Element appellateBrief : appellateBriefRowElementList) {
            try {
                final String briefData = appellateBrief.selectFirst("td.rgSorted").text();
                log.info(briefData);
            } catch (NullPointerException ex) {
                log.info("Selector not found");
            }
        }
    }
}
