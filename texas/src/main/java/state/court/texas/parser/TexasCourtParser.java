package state.court.texas.parser;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;
import state.court.model.MetaData;
import state.court.utils.ParserUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static state.court.constant.Constants.DATE_PATTERN;
import static state.court.constant.Constants.LINK_ATTR_SELECTOR;
import static state.court.texas.constant.TexasCourtConstants.*;

/**
 * @author foxy
 * @since 09.04.19.
 */
@Slf4j
@Component
public class TexasCourtParser {

    public List<MetaData> parsePage(String page, List<WebElement> caseNumberList) {
        List<MetaData> metaDataList = new ArrayList<>();
        Document mainCaseDocument = Jsoup.parse(page);
        if (mainCaseDocument != null) {
            for (int i = 0; i < caseNumberList.size(); i++) {
                WebElement caseNumberUrl = caseNumberList.get(i);
                final String url = caseNumberUrl.getAttribute(LINK_ATTR_SELECTOR);
                String caseName = parseCaseName(mainCaseDocument, i);
                String caseNumber = parseCaseNumber(mainCaseDocument, i);
                metaDataList.addAll(parseMetadata(caseName, caseNumber, url));
            }
        }
        return metaDataList;
    }

    private String parseCaseName(Document caseDocument, int i) {
        Element caseName = caseDocument.selectFirst("#ctl00_ContentPlaceHolder1_grdEBrief_ctl00__" + i + " > td:nth-child(2)");
        return caseName.text();
    }

    private String parseCaseNumber(Document caseDocument, int i) {
        int j = i + 1;
        Element caseNumber = caseDocument.selectFirst("#ctl00_ContentPlaceHolder1_grdEBrief_ctl00 tr:nth-child(" + j + ") td a");
        return caseNumber.text();
    }

    private List<MetaData> parseMetadata(String caseName, String caseNumber, String url) {
        List<MetaData> metaDataList = new ArrayList<>();
        Document document = getCaseDocument(url);
        Elements appellateBriefsRows = document.select(APPELLATE_ROW_SELECTOR);
        for (Element appellateBriefsRow : appellateBriefsRows) {
            metaDataList.addAll(parseAppellateBriefRow(appellateBriefsRow, caseName, caseNumber));
        }
        return metaDataList;
    }

    private Document getCaseDocument(String url) {
        Document caseDocument;
        try {
            caseDocument = Jsoup.connect(url).timeout(TIMEOUT).get();
        } catch (IOException e) {
            throw new IllegalArgumentException("Can't get main page: " + url);
        }
        return caseDocument;
    }

    private String parseDescription(Element elementRow) {
        String remarkPart = elementRow.selectFirst(APPELLATE_REMARK_PART_SELECTOR).text();
        String datePart = elementRow.selectFirst(APPELLATE_DATE_PART_SELECTOR).text();
        return remarkPart + " " + datePart;
    }

    private List<MetaData> parseAppellateBriefRow(Element appellateBriefsRow, String caseName, String caseNumber) {
        List<MetaData> metaDataList = new ArrayList<>();
        final List<String> appellateBriefUrlList = new ArrayList<>();
        String briefData = StringUtils.EMPTY;
        String description = StringUtils.EMPTY;
        Elements appellateBriefDocumentColumnElementList = appellateBriefsRow.select(APPELLATE_LINK_PART_SELECTOR);
        for (Element appellateBrief : appellateBriefDocumentColumnElementList) {
            final String urlCut = appellateBrief.attr(LINK_ATTR_SELECTOR);
            String caseUrl = MAIN_URL_PART + urlCut;
            appellateBriefUrlList.add(caseUrl);
        }
        try {
            briefData = appellateBriefsRow.selectFirst(APPELLATE_DATE_PART_SELECTOR).text();
            description = parseDescription(appellateBriefsRow);
        } catch (NullPointerException ex) {
            log.info("Selector not found {}", caseNumber);
        }

        for (int i = 0; i < appellateBriefUrlList.size(); i++) {
            MetaData metaData = new MetaData();
            metaData.setFileName(ParserUtils.getFilename(caseNumber, caseName, i));
            metaData.setCaseName(caseName);
            metaData.setDescription(description);
            metaData.setJurisdiction(TEXAS_JURISDICTION);
            metaData.setDocketOrCaseNumber(caseNumber);
            metaData.setBriefField(ParserUtils.formatBriefField(briefData));
            metaData.setRetrieved(LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_PATTERN)));
            metaData.setAppellateBriefUrl(appellateBriefUrlList.get(i));
            metaDataList.add(metaData);
        }
        return metaDataList;
    }
}
