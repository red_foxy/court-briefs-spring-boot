package state.court.texas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import state.court.service.CourtService;
import state.court.texas.scraper.TexasCourtScraper;

import javax.transaction.Transactional;
import java.io.IOException;

/**
 * @author foxy
 * @since 22.11.18.
 */
@Service
public class TexasCourtServiceImpl implements CourtService {

    private final TexasCourtScraper texasScraper;

    @Autowired
    public TexasCourtServiceImpl(TexasCourtScraper texasScraper) {
        this.texasScraper = texasScraper;
    }

    @Override
    @Transactional
    public void scrapeCourt() throws IOException {
        texasScraper.parsePage();
    }
}
