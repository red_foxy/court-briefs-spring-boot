package state.court.texas.constant;

/**
 * @author foxy
 * @since 09.04.19.
 */
public class TexasCourtConstants {

    public static final String MAIN_URL = "http://search.txcourts.gov/ebriefs.aspx?y=2018&coa=cossup";
    public static final String MAIN_URL_PART = "http://search.txcourts.gov/";
    public static final String SUB_DIR_NAME = "texas";
    public static final String SUB_DIR_NAME_UPDATE = "/data/" + SUB_DIR_NAME;
    public static final String EXTRA_LINK = "parental-notification";
    public static final String YEAR_LINK_SELECTOR = "#ctl00_ContentPlaceHolder1_pnlEvents > p:nth-child(2) > a";
    public static final String TEXAS_JURISDICTION = "Texas Supreme Court";
    public static final String CASE_NUMBER_LINK_SELECTOR = "#ctl00_ContentPlaceHolder1_grdEBrief_ctl00 td a";
    public static final String APPELLATE_ROW_SELECTOR = "#ctl00_ContentPlaceHolder1_grdBriefs_ctl00 > tbody > tr";
    public static final String APPELLATE_REMARK_PART_SELECTOR = "td:nth-child(4)";
    public static final String APPELLATE_DATE_PART_SELECTOR = "td.rgSorted";
    public static final String APPELLATE_LINK_PART_SELECTOR = "tr:not(:has(td:contains(Notice))) a";
    public static final Integer TIMEOUT = 100 * 1000;

    private TexasCourtConstants() {
    }
}
