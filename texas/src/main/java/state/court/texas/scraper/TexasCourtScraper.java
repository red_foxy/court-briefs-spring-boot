package state.court.texas.scraper;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;
import state.court.driver.WebDriverWrapper;
import state.court.entity.FileNames;
import state.court.entity.ScraperInfo;
import state.court.io.FileOutputImpl;
import state.court.model.MetaData;
import state.court.repository.FileNameRepository;
import state.court.repository.ScraperRepository;
import state.court.texas.parser.TexasCourtParser;
import state.court.utils.ParserUtils;
import state.court.utils.ZipUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static state.court.constant.Constants.LINK_ATTR_SELECTOR;
import static state.court.texas.constant.TexasCourtConstants.*;

/**
 * @author foxy
 * @since 22.11.18.
 */
@Component
public class TexasCourtScraper {

    private final ScraperRepository scraperRepository;
    private final FileNameRepository fileNameRepository;
    private final TexasCourtParser texasParser;

    @Autowired
    public TexasCourtScraper(ScraperRepository scraperRepository,
                             FileNameRepository fileNameRepository,
                             TransactionTemplate transactionTemplate, TexasCourtParser texasParser) {
        this.scraperRepository = scraperRepository;
        this.fileNameRepository = fileNameRepository;
        this.texasParser = texasParser;
    }

    public void parsePage() throws IOException {
        ScraperInfo scraperInfo = scraperRepository.save(ParserUtils.saveScraperInfo(SUB_DIR_NAME));
        String currentYear = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
        String previousYear = Integer.toString(Calendar.getInstance().get(Calendar.YEAR) - 1);
        final List<String> yearUrlList = new ArrayList<>();
        try (final WebDriverWrapper webDriver = new WebDriverWrapper()) {
            webDriver.get(MAIN_URL);
            final List<WebElement> yearElementList = webDriver.findElements(By.cssSelector(YEAR_LINK_SELECTOR));
            for (WebElement elementUrl : yearElementList) {
                final String url = elementUrl.getAttribute(LINK_ATTR_SELECTOR);
                if (!getExistFiles(scraperInfo).isEmpty()) {
                    if (url.contains(currentYear) || url.contains(previousYear)) {
                        yearUrlList.add(url);
                    }
                } else {
                    if (!url.contains(EXTRA_LINK)) {
                        yearUrlList.add(url);
                    }
                }
            }
            parsePageByUrl(yearUrlList, scraperInfo);
        }
        ZipUtils.archiveFiles(SUB_DIR_NAME, SUB_DIR_NAME_UPDATE);
    }

    private void parsePageByUrl(List<String> yearUrlList, ScraperInfo scraperInfo) throws IOException {
        try (final WebDriverWrapper webDriver = new WebDriverWrapper()) {
            for (String yearUrl : yearUrlList) {
                webDriver.get(yearUrl);
                final List<WebElement> caseNumberList = webDriver.findElements(By.cssSelector(CASE_NUMBER_LINK_SELECTOR));
                String pageSource = webDriver.getPageSource();
                List<String> fileNames = new ArrayList<>();
                final List<MetaData> metaDataList = texasParser.parsePage(pageSource, caseNumberList);
                for (MetaData metaData : metaDataList) {
                    final List<String> downloadPDFList = downloadPDF(metaData, scraperInfo);
                    if (!downloadPDFList.isEmpty())
                        fileNames.addAll(downloadPDFList);
                }
                ParserUtils.setFileNameToScraperInfo(fileNames, scraperInfo);
                scraperRepository.save(scraperInfo);
            }
        }
    }

    private List<String> downloadPDF(MetaData metaData, ScraperInfo scraperInfo) throws IOException {
        List<String> existedFiles = getExistFiles(scraperInfo);
        FileOutputImpl output = new FileOutputImpl(SUB_DIR_NAME);
        return output.downloadPDF(metaData, SUB_DIR_NAME, existedFiles, scraperInfo);
    }

    private List<String> getExistFiles(ScraperInfo scraperInfo) {
        List<String> existingFiles = new ArrayList<>();
        List<FileNames> fileNames = fileNameRepository.findAllByScraperInfoScraperName(scraperInfo.getScraperName());
        for (FileNames file : fileNames) {
            existingFiles.add(file.getFileName());
        }
        return existingFiles;
    }
}
