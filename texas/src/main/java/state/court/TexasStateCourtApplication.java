package state.court;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import state.court.service.CourtService;

import javax.transaction.Transactional;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class TexasStateCourtApplication implements CommandLineRunner
{
    private final CourtService service;

    @Autowired
    public TexasStateCourtApplication(@Qualifier("texasCourtServiceImpl") CourtService texasService) {
        this.service = texasService;
    }

    public static void main( String[] args )
    {
        SpringApplication.run(TexasStateCourtApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        service.scrapeCourt();
    }
}
