package state.court.massachusetts.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import state.court.service.CourtService;
import state.court.massachusetts.scraper.MassachusettsCourtScraper;

import java.io.IOException;

/**
 * @author foxy
 * @since 03.01.19.
 */
@Service
public class MassachusettsCourtServiceImpl implements CourtService {

    private final MassachusettsCourtScraper massachusettsScraper;

    @Autowired
    public MassachusettsCourtServiceImpl(MassachusettsCourtScraper massachusettsScraper) {
        this.massachusettsScraper = massachusettsScraper;
    }

    @Override
    @Transactional
    public void scrapeCourt() throws IOException {
        massachusettsScraper.scrapePage();
    }
}
