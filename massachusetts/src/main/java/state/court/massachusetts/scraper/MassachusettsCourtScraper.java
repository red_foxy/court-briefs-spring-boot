package state.court.massachusetts.scraper;


import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import state.court.driver.WebDriverWrapper;
import state.court.entity.FileNames;
import state.court.entity.ScraperInfo;
import state.court.io.FileOutputImpl;
import state.court.massachusetts.parser.MassachusettsCourtParser;
import state.court.model.MetaData;
import state.court.repository.FileNameRepository;
import state.court.repository.ScraperRepository;
import state.court.utils.ParserUtils;
import state.court.utils.ZipUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static state.court.constant.Constants.LINK_ATTR_SELECTOR;
import static state.court.massachusetts.constant.MassachusettsCourtConstants.*;

/**
 * @author foxy
 * @since 28.12.18.
 */
@Slf4j
@Component
public class MassachusettsCourtScraper {

    private final ScraperRepository scraperRepository;
    private final FileNameRepository fileNameRepository;
    private final MassachusettsCourtParser massachusettsParser;

    @Autowired
    public MassachusettsCourtScraper(ScraperRepository scraperRepository,
                                     FileNameRepository fileNameRepository,
                                     MassachusettsCourtParser massachusettsParser) {
        this.scraperRepository = scraperRepository;
        this.fileNameRepository = fileNameRepository;
        this.massachusettsParser = massachusettsParser;
    }

    public void scrapePage() throws IOException {
        ScraperInfo scraperInfo = scraperRepository.save(ParserUtils.saveScraperInfo(SUB_DIR_NAME));
        final List<String> briefUrlList = new ArrayList<>();
        try (final WebDriverWrapper webDriver = new WebDriverWrapper()) {
            for (String url : initializeUrlList()) {
                webDriver.get(url);
                final List<WebElement> briefElementList = webDriver.findElements(
                        By.cssSelector(CASE_NUMBER_SELECTOR));
                for (WebElement elementUrl : briefElementList) {
                    briefUrlList.add(elementUrl.getAttribute(LINK_ATTR_SELECTOR));
                }
            }
            parseCaseNumberPage(briefUrlList, scraperInfo, webDriver);
        }
        ZipUtils.archiveFiles(SUB_DIR_NAME, SUB_DIR_NAME_UPDATE);
    }

    private List<String> initializeUrlList() {
        final List<String> mainUrlList = new ArrayList<>();
        mainUrlList.add("http://ma-appellatecourts.org/display_calendar.php?dtp=fc");
        mainUrlList.add("http://ma-appellatecourts.org/display_calendar.php?dtp=sj");
        mainUrlList.add("http://ma-appellatecourts.org/display_calendar.php?dtp=ac");
        mainUrlList.add("http://ma-appellatecourts.org/display_calendar.php?dtp=aj");
        return mainUrlList;
    }

    private void parseCaseNumberPage(List<String> briefUrlList, ScraperInfo scraperInfo, WebDriverWrapper webDriver) throws IOException {
        List<String> fileNames = new ArrayList<>();
        for (String url : briefUrlList) {
            webDriver.delay(4000, 4500);
            webDriver.get(url);
            webDriver.delay(4000, 4500);
            try {
                String pageSource = webDriver.getPageSource();
                final List<MetaData> metaDataList = massachusettsParser.parsePage(pageSource);
                for (MetaData metaData : metaDataList) {
                    fileNames.addAll(downloadPDF(metaData, scraperInfo));
                }
            } catch (NullPointerException ex) {
                log.error("Searching engine limit finished");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    log.error("{}", e.getMessage());
                    Thread.currentThread().interrupt();
                }
            }
        }
        ParserUtils.setFileNameToScraperInfo(fileNames, scraperInfo);
        scraperRepository.save(scraperInfo);
    }

    private List<String> downloadPDF(MetaData metaData, ScraperInfo scraperInfo) throws IOException {
        FileOutputImpl output = new FileOutputImpl(SUB_DIR_NAME);
        List<String> existedFiles = getExistFiles(scraperInfo, output);
        return output.downloadPDF(metaData, SUB_DIR_NAME, existedFiles, scraperInfo);
    }

    private List<String> getExistFiles(ScraperInfo scraperInfo, FileOutputImpl output) {
        List<FileNames> fileNames = fileNameRepository.findAllByScraperInfoScraperName(scraperInfo.getScraperName());
        return output.getExistFiles(fileNames);
    }
}
