package state.court.massachusetts.parser;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import state.court.model.MetaData;
import state.court.utils.ParserUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static state.court.constant.Constants.DATE_PATTERN;
import static state.court.constant.Constants.LINK_ATTR_SELECTOR;
import static state.court.massachusetts.constant.MassachusettsCourtConstants.*;

/**
 * @author foxy
 * @since 01.04.19.
 */
@Slf4j
@Component
public class MassachusettsCourtParser {

    public List<MetaData> parsePage(String page) {
        List<MetaData> metaDataList = new ArrayList<>();
        Document document = Jsoup.parse(page);
        if (document != null) {
            metaDataList.addAll(parseCaseNumberParagraph(document));
        }
        return metaDataList;
    }

    private List<MetaData> parseCaseNumberParagraph(Document document) {
        List<MetaData> metaDataList = new ArrayList<>();
        List<String> briefDescriptionList = parseDescription(document);
        List<String> appellateBriefList = parseAppellateBrief(document);
        String caseName = parseCaseName(document);
        String caseNumber = parseCaseNumber(document);
        String briefField = parseFieldBrief(document);
        if (appellateBriefList.size() == briefDescriptionList.size()) {
            for (int i = 0; i < appellateBriefList.size(); i++) {
                MetaData metaData = new MetaData();
                metaData.setFileName(ParserUtils.getFilename(caseNumber, caseName, i));
                metaData.setJurisdiction(parseJurisdiction(document));
                metaData.setCaseName(caseName);
                metaData.setDocketOrCaseNumber(caseNumber);
                metaData.setBriefField(ParserUtils.formatBriefField(briefField));
                metaData.setDescription(briefDescriptionList.get(i));
                metaData.setRetrieved(LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_PATTERN)));
                metaData.setAppellateBriefUrl(appellateBriefList.get(i));
                metaDataList.add(metaData);
            }
        }
        return metaDataList;
    }

    private String parseCaseName(Document document) {
        return document.selectFirst(CASE_NAME_SELECTOR).text().trim();
    }

    private String parseCaseNumber(Document document) {
        String dirtyCaseParagraph = document.selectFirst(CASE_DIRTY_NUMBER_SELECTOR)
                .text().trim();
        String caseNumber = StringUtils.EMPTY;
        Pattern pattern = Pattern.compile(CASE_NUMBER_PATTERN);
        Matcher matcher = pattern.matcher(dirtyCaseParagraph);
        if (matcher.find()) {
            caseNumber = matcher.group();
        }
        return caseNumber;
    }

    private String parseJurisdiction(Document document) {
        String jurisdiction = document.selectFirst(JURISDICTION_SELECTOR).text().trim();
        if (StringUtils.isBlank(jurisdiction)) {
            jurisdiction = JURISDICTION_DEFAULT;
        }
        return jurisdiction;
    }

    private List<String> parseDescription(Document caseDocument) {
        List<String> briefDescriptionList = new ArrayList<>();
        Elements tables = caseDocument.select(DESCRIPTION_SELECTOR);
        for (Element table : tables) {
            String description = table.text().trim();
            if (StringUtils.isNotBlank(description))
                briefDescriptionList.add(description);
        }
        return briefDescriptionList;
    }

    private List<String> parseAppellateBrief(Document caseDocument) {
        Set<String> appellateBriefSet = new HashSet<>();
        Elements tables = caseDocument.select(DESCRIPTION_SELECTOR);
        for (Element table : tables) {
            String appellateBriefUrl = table.attr(LINK_ATTR_SELECTOR).trim();
            appellateBriefSet.add(appellateBriefUrl);
        }
        return new ArrayList<>(appellateBriefSet);
    }

    private String parseFieldBrief(Document caseDocument) {
        Elements briefFieldElement = caseDocument.select(BRIEF_FIELD_SELECTOR);
        String briefField = briefFieldElement.text().trim();
        Pattern pattern = Pattern.compile(BRIEF_FIELD_PATTERN);
        return ParserUtils.getString(briefField, pattern);
    }
}
