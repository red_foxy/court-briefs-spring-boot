package state.court.massachusetts.constant;

/**
 * @author foxy
 * @since 01.04.19.
 */
public class MassachusettsCourtConstants {

    public static final String SUB_DIR_NAME = "massachusetts";
    public static final String CASE_NUMBER_SELECTOR = ".home table tr[valign=\"top\"] a[target=\"_top\"]";
    public static final String SUB_DIR_NAME_UPDATE = "/data/" + SUB_DIR_NAME;
    public static final String CASE_NAME_SELECTOR = "td.homecell > table > tbody > tr > td > table:nth-child(1) > tbody > tr:nth-child(3) > td > b";
    public static final String CASE_DIRTY_NUMBER_SELECTOR = "td.homecell > table > tbody > tr > td > table:nth-child(1) > tbody > tr:nth-child(3) > td";
    public static final String CASE_NUMBER_PATTERN = "(?=((([A-Z]*)|(\\d{0,4}))-(\\d{3,5}))|(\\d{0,4}-)).*";
    public static final String JURISDICTION_SELECTOR = "td.homecell > table > tbody > tr > td > table:nth-child(1) > tbody > tr:nth-child(2) > td > b";
    public static final String JURISDICTION_DEFAULT = "Massachusetts Court of Appeals";
    public static final String DESCRIPTION_SELECTOR = "td.homecell > table > tbody > tr > td > table:contains(Documents) a";
    public static final String BRIEF_FIELD_PATTERN = "\\d{2}/\\d{2}/\\d{4}";
    public static final String BRIEF_FIELD_SELECTOR = "table > tbody > tr > td > table:nth-child(2) > tbody > tr:contains(Entry Date) td:contains(/)";

    private MassachusettsCourtConstants() {
        throw new IllegalStateException("Utility class");
    }
}
