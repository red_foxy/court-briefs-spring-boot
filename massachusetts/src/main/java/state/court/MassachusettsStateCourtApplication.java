package state.court;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;
import state.court.service.CourtService;


/**
 * Hello world!
 *
 */
@SpringBootApplication
public class MassachusettsStateCourtApplication implements CommandLineRunner
{

    private final CourtService service;

    @Autowired
    public MassachusettsStateCourtApplication(@Qualifier("massachusettsCourtServiceImpl") CourtService service) {
        this.service = service;
    }

    public static void main( String[] args )
    {
        SpringApplication.run(MassachusettsStateCourtApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        service.scrapeCourt();
    }
}
