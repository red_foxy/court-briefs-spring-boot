package state.court.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import state.court.service.CourtService;
import state.court.scraper.NorthCarolinaCourtScraper;

import java.io.IOException;

/**
 * @author foxy
 * @since 26.02.19.
 */
@Service
public class NorthCarolinaCourtServiceImpl implements CourtService {

    private final NorthCarolinaCourtScraper northCarolinaScraper;

    @Autowired
    public NorthCarolinaCourtServiceImpl(NorthCarolinaCourtScraper northCarolinaScraper) {
        this.northCarolinaScraper = northCarolinaScraper;
    }

    @Override
    public void scrapeCourt() throws IOException {
        northCarolinaScraper.parsePage();
    }
}
