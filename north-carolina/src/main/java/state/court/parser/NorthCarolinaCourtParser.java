package state.court.parser;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import state.court.dao.Page;
import state.court.model.MetaData;
import state.court.utils.ParserUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static state.court.constant.Constants.DATE_PATTERN;
import static state.court.constant.Constants.LINK_ATTR_SELECTOR;
import static state.court.constant.NorthCarolinaCourtConstants.*;

/**
 * @author foxy
 * @since 27.02.19.
 */
@Component
public class NorthCarolinaCourtParser {

    public List<MetaData> parsePages(List<Page> pages) {
        List<MetaData> metaDataList = new ArrayList<>();
        for (Page page : pages) {
            Document document = Jsoup.parse(page.getPageSource());
            Elements trList = document.select(TR_SELECTOR);
            String currentHeader = null;
            for (Element tr : trList) {
                if (isHeaderTr(tr)) {
                    currentHeader = tr.text();
                } else if (currentHeader != null && isMetaDataTr(tr)) {
                    metaDataList.addAll(parseMetaDataTr(page.getCourt(), currentHeader, tr));
                }
            }
        }
        return metaDataList;
    }

    private boolean isHeaderTr(Element tr) {
        return (!tr.select(TR_HEADER_SELECTOR).isEmpty() && (!tr.text().contains(DIRTY_TEXT_HEADER))
                && (!tr.text().contains(DIRTY_TEXT_RESTRICTED_HEADER)));
    }

    private boolean isMetaDataTr(Element tr) {
        return ((tr.childNodeSize() == 4) && (!(tr.selectFirst(METADATA_TR_SELECTOR)).text().contains(METADATA_TR_TEXT)));
    }

    private List<MetaData> parseMetaDataTr(String court, String currentHeader, Element tr) {
        List<MetaData> metaDataList = new ArrayList<>();
        String caseName = parseCaseName(currentHeader);
        String caseNumber = parseCaseNumber(currentHeader);
        List<String> briefFieldList = parseFieldBrief(tr);
        List<String> appellateBriefUrlList = parseAppellateBrief(tr);
        if (appellateBriefUrlList.size() == briefFieldList.size()) {
            for (int i = 0; i < appellateBriefUrlList.size(); i++) {
                MetaData metaData = new MetaData();
                metaData.setFileName(ParserUtils.getFilename(caseNumber, caseName, i));
                metaData.setCaseName(caseName);
                metaData.setJurisdiction(court);
                metaData.setDocketOrCaseNumber(caseNumber);
                metaData.setBriefField(briefFieldList.get(i));
                metaData.setRetrieved(LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_PATTERN)));
                metaData.setAppellateBriefUrl(appellateBriefUrlList.get(i));
                metaDataList.add(metaData);
            }
        }
        return metaDataList;
    }

    private String parseCaseName(String currentHeader) {
        return StringUtils.substringAfter(currentHeader, SEPARATOR).trim();
    }

    private String parseCaseNumber(String currentHeader) {
        return StringUtils.substringBefore(currentHeader, SEPARATOR).trim();
    }

    private List<String> parseFieldBrief(Element tr) {
        final List<String> briefFieldList = new ArrayList<>();
        Elements briefFieldElements = tr.select(BRIEF_FIELD_SELECTOR);
        for (Element briefFieldElement : briefFieldElements) {
            String briefField = ParserUtils.formatBriefField(briefFieldElement.text().trim());
            briefFieldList.add(briefField);
        }
        return briefFieldList;
    }

    private List<String> parseAppellateBrief(Element tr) {
        List<String> appellateBriefUrlList = new ArrayList<>();
        Elements appellateBriefElements = tr.select(APPELLATE_BRIEF_SELECTOR);
        for (Element appellateBriefElement : appellateBriefElements) {
            String appellateBriefUrl = appellateBriefElement.attr(LINK_ATTR_SELECTOR);
            appellateBriefUrlList.add(appellateBriefUrl);
        }
        return appellateBriefUrlList;
    }
}
