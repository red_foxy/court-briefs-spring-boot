package state.court.scraper;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import state.court.dao.Page;
import state.court.driver.ProxyWebDriverWrapper;
import state.court.entity.FileNames;
import state.court.entity.ScraperInfo;
import state.court.io.FileOutputImpl;
import state.court.model.MetaData;
import state.court.parser.NorthCarolinaCourtParser;
import state.court.repository.FileNameRepository;
import state.court.repository.ScraperRepository;
import state.court.utils.ParserUtils;
import state.court.utils.ZipUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static state.court.constant.Constants.PROXY_HOST;
import static state.court.constant.Constants.PROXY_PORT;
import static state.court.constant.NorthCarolinaCourtConstants.*;

/**
 * @author foxy
 * @since 26.02.19.
 */
@Component
public class NorthCarolinaCourtScraper {
    private final ScraperRepository scraperRepository;
    private final NorthCarolinaCourtParser northCarolinaParser;
    private final FileNameRepository fileNameRepository;

    @Autowired
    public NorthCarolinaCourtScraper(ScraperRepository scraperRepository,
                                     NorthCarolinaCourtParser northCarolinaParser, FileNameRepository fileNameRepository) {
        this.scraperRepository = scraperRepository;
        this.northCarolinaParser = northCarolinaParser;
        this.fileNameRepository = fileNameRepository;
    }

    public void parsePage() throws IOException {
        ScraperInfo scraperInfo = scraperRepository.save(ParserUtils.saveScraperInfo(SUB_DIR_NAME));
        startSearch(scraperInfo);
    }

    private void startSearch(ScraperInfo scraperInfo) throws IOException {
        try (final ProxyWebDriverWrapper webDriver = new ProxyWebDriverWrapper()) {
            List<Page> searchResultPages = inputSearchValue(webDriver);
            startParsePages(searchResultPages, scraperInfo);
        }
        ZipUtils.archiveFiles(SUB_DIR_NAME, SUB_DIR_NAME_UPDATE);
    }

    private List<Page> inputSearchValue(ProxyWebDriverWrapper webDriver) {
        List<Page> searchResultPages = new ArrayList<>();
        By nextPageSelector = By.xpath(NEXT_PAGE_XPATH);
        for (String fileType : initializeFileTypeDropdown()) {
            for (String court : initializeCourtDropdown()) {
                webDriver.get(MAIN_URL);
                Select dropdownFileType = new Select((webDriver.findElement(By.name(FILE_TYPE_DROPDOWN))));
                Select dropdownCourt = new Select((webDriver.findElement(By.name(COURT_DROPDOWN))));
                dropdownFileType.selectByVisibleText(fileType);
                dropdownCourt.selectByVisibleText(court);
                clickSearchButton(webDriver);
                webDriver.delay(3000, 6000);
                while (webDriver.exists(nextPageSelector)) {
                    webDriver.delay(3000, 6000);
                    Page page = new Page();
                    page.setPageSource(webDriver.getPageSource());
                    page.setCourt(court);
                    searchResultPages.add(page);
                    webDriver.findElement(nextPageSelector).click();
                }
            }
        }
        return searchResultPages;
    }

    private List<String> initializeFileTypeDropdown() {
        List<String> fileTypes = new ArrayList<>();
        fileTypes.add("Appellant Brief");
        fileTypes.add("Appellee Brief");
        fileTypes.add("Other Brief");
        return fileTypes;
    }

    private List<String> initializeCourtDropdown() {
        List<String> courts = new ArrayList<>();
        courts.add("Supreme Court");
        courts.add("Court Of Appeals");
        return courts;
    }

    private void clickSearchButton(ProxyWebDriverWrapper webDriver) {
        By searchButtonSelector = By.cssSelector(SEARCH_BUTTON_SELECTOR);
        if (webDriver.exists(searchButtonSelector)) {
            webDriver.findElement(searchButtonSelector).click();
        }
    }

    private void startParsePages(List<Page> searchResultPages, ScraperInfo scraperInfo) throws IOException {
        List<String> fileNames = new ArrayList<>();
        List<MetaData> metaDataList = northCarolinaParser.parsePages(searchResultPages);
        for (MetaData metaData : metaDataList) {
            fileNames.addAll(downloadPDF(metaData, scraperInfo));
        }
        ParserUtils.setFileNameToScraperInfo(fileNames, scraperInfo);
        scraperRepository.save(scraperInfo);
    }

    private List<String> downloadPDF(MetaData metaData, ScraperInfo scraperInfo) throws IOException {
        FileOutputImpl output = new FileOutputImpl(SUB_DIR_NAME);
        List<String> existedFiles = getExistFiles(scraperInfo, output);
        return output.downloadPDFWithProxy(metaData, SUB_DIR_NAME, existedFiles, scraperInfo, PROXY_HOST, PROXY_PORT);
    }

    private List<String> getExistFiles(ScraperInfo scraperInfo, FileOutputImpl output) {
        List<FileNames> fileNames = fileNameRepository.findAllByScraperInfoScraperName(scraperInfo.getScraperName());
        return output.getExistFiles(fileNames);
    }
}
