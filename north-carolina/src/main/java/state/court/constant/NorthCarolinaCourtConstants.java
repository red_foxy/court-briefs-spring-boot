package state.court.constant;

/**
 * @author foxy
 * @since 08.04.19.
 */
public class NorthCarolinaCourtConstants {
    public static final String SUB_DIR_NAME = "north-carolina";
    public static final String SUB_DIR_NAME_UPDATE = "/data/" + SUB_DIR_NAME;
    public static final String NEXT_PAGE_XPATH = "//*[@id=\"content\"]/a[text()[contains(.,'Next Page')]]";
    public static final String MAIN_URL = "https://www.ncappellatecourts.org/";
    public static final String FILE_TYPE_DROPDOWN = "type";
    public static final String COURT_DROPDOWN = "court_name";
    public static final String TR_SELECTOR = "#content > table > tbody > tr";
    public static final String TR_HEADER_SELECTOR = "span[style]";
    public static final String DIRTY_TEXT_HEADER = "Test";
    public static final String DIRTY_TEXT_RESTRICTED_HEADER = "Restricted";
    public static final String METADATA_TR_SELECTOR = "td:nth-child(2)";
    public static final String METADATA_TR_TEXT = "AIS";
    public static final String SEPARATOR = ":";
    public static final String BRIEF_FIELD_SELECTOR = "td:nth-child(4)";
    public static final String APPELLATE_BRIEF_SELECTOR = "td > a";
    public static final String SEARCH_BUTTON_SELECTOR = "#content > center > form > table > tbody > tr > td > " +
            " input[type=\"submit\"]";

    private NorthCarolinaCourtConstants() {
    }
}
