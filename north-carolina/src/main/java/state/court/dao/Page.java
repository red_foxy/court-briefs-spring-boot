package state.court.dao;

import lombok.*;

/**
 * @author foxy
 * @since 28.02.19.
 */
@Data
public class Page {

    private String pageSource;
    private String court;
}
