package state.court;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import state.court.service.CourtService;

/**
 * @author foxy
 * @since 25.02.19.
 */
@SpringBootApplication
public class NorthCarolinaSateCourtApplication implements CommandLineRunner {

    private final CourtService service;

    @Autowired
    public NorthCarolinaSateCourtApplication(@Qualifier("northCarolinaCourtServiceImpl") CourtService service) {
        this.service = service;
    }

    public static void main(String[] args) {
        SpringApplication.run(NorthCarolinaSateCourtApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        service.scrapeCourt();
    }
}
