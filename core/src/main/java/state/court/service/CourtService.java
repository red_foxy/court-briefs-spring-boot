package state.court.service;

import java.io.IOException;

/**
 * @author foxy
 * @since 18.03.19.
 */
public interface CourtService {

    void scrapeCourt() throws IOException;
}
