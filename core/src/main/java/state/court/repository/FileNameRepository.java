package state.court.repository;

import state.court.entity.FileNames;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author foxy
 * @since 04.02.19.
 */
@Repository
public interface FileNameRepository extends JpaRepository<FileNames, Long> {

    List<FileNames> findAllByScraperInfoScraperName(String scraperName);
}
