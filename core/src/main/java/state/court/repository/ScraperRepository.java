package state.court.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import state.court.entity.ScraperInfo;

/**
 * @author foxy
 * @since 31.01.19.
 */
@Repository
public interface ScraperRepository extends JpaRepository<ScraperInfo, Long> {
}
