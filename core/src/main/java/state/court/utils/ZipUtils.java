package state.court.utils;

import lombok.extern.slf4j.Slf4j;
import state.court.io.FileOutputImpl;
import state.court.io.TarApacheCompressUtil;

import java.io.File;
import java.io.IOException;

import static state.court.constant.Constants.USER_DIRECTORY;

/**
 * @author foxy
 * @since 18.03.19.
 */
@Slf4j
public class ZipUtils {

    public static void archiveFiles(String subDirName, String subDirNameUpdate) throws IOException {
        FileOutputImpl output = new FileOutputImpl(subDirName);
        String subDir = USER_DIRECTORY + subDirNameUpdate + File.separator;
        TarApacheCompressUtil.compressFiles(subDir);
        output.tarIt(subDir);
        log.info("Tar file completed");
    }

    private ZipUtils() {
    }
}
