package state.court.utils;

import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import state.court.driver.WebDriverWrapper;

import java.util.List;

/**
 * @author foxy
 * @since 22.04.19.
 */
public class ScraperUtils {

    public static void scrapeBriefLinkList(List<String> briefUrlList, WebDriverWrapper webDriver, String briefLinkSelector, String linkAttrSelector) {
        final List<WebElement> briefElementList = webDriver.findElements(By.cssSelector(briefLinkSelector));
        for (WebElement elementUrl : briefElementList) {
            final String url = elementUrl.getAttribute(linkAttrSelector);
            briefUrlList.add(url);
        }
    }

    public static String scrapeBriefLink(Document mainDocument, String briefLinkSelector, String linkAttrSelector) {
        return mainDocument.select(briefLinkSelector).attr(linkAttrSelector);
    }

    private ScraperUtils() {
    }
}
