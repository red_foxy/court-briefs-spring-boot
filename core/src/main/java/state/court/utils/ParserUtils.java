package state.court.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import state.court.entity.FileNames;
import state.court.entity.ScraperInfo;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static state.court.constant.Constants.*;
import static state.court.constant.Constants.UNDERSCORE;

/**
 * @author foxy
 * @since 18.03.19.
 */
@Slf4j
public class ParserUtils {

    private ParserUtils() {
    }

    public static void setFileNameToScraperInfo(List<String> fileNames, ScraperInfo scraperInfo) {
        if (!fileNames.isEmpty()) {
            scraperInfo.getFileNameList().clear();
            scraperInfo.getFileNameList().addAll(generateFileNames(fileNames, scraperInfo));
            scraperInfo.getFileNameList().forEach(fl -> fl.setScraperInfo(scraperInfo));
        }
    }

    private static List<FileNames> generateFileNames(List<String> fileNames, ScraperInfo scraperInfo) {
        List<FileNames> fileNamesList = new ArrayList<>();
        for (String file : fileNames) {
            FileNames fileName = new FileNames();
            fileName.setFileName(file);
            fileName.setScraperInfo(scraperInfo);
            fileNamesList.add(fileName);
        }
        return fileNamesList;
    }

    public static ScraperInfo saveScraperInfo(String subDirName) {
        ScraperInfo scraperInfo = new ScraperInfo();
        LocalDateTime dateTime = LocalDateTime.now();
        scraperInfo.setScraperName(subDirName);
        scraperInfo.setStartDate(Timestamp.valueOf(dateTime));
        return (scraperInfo);
    }

    public static List<String> getStrings(String text, List<String> list, Pattern pattern, int countGeneralLink) {
        Matcher matcher = pattern.matcher(text);
        for (int i = 0; i < countGeneralLink; i++) {
            if (matcher.find()) {
                final String group = matcher.group();
                list.add(group);
            } else {
                list.add(StringUtils.EMPTY);
            }
        }
        return list;
    }

    public static List<String> getStrings(String text, List<String> briefFieldList, Pattern pattern) {
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            final String group = matcher.group();
            briefFieldList.add(group);
        }
        return briefFieldList;
    }

    public static String getString(String text, Pattern pattern) {
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            return matcher.group();
        }
        return StringUtils.EMPTY;
    }

    public static String getCurrentYear() {
        return Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
    }

    public static String getPreviousYear() {
        return Integer.toString(Calendar.getInstance().get(Calendar.YEAR) - 1);
    }

    public static String formatBriefField(String briefField) {
        if (StringUtils.isBlank(briefField)) {
            return StringUtils.EMPTY;
        }
        DateTimeFormatter oldPattern = DateTimeFormatter.ofPattern(DATE_PATTERN_LIST);
        DateTimeFormatter newPattern;
        LocalDate datetime;
        try {
            newPattern = DateTimeFormatter.ofPattern(DATE_PATTERN);
            datetime = LocalDate.parse(briefField, oldPattern);
        } catch (DateTimeParseException ex) {
            log.info("Bad format {}", briefField);
            return StringUtils.EMPTY;
        }
        return datetime.format(newPattern);
    }

    public static String getFilename(String caseNumber, String caseName, Integer number) {
        if (number != 0)
            return String.format(FILENAME_FORMATTER_NUMBER, caseNumber.toLowerCase(), caseName.toLowerCase(), number)
                    .replaceAll(StringUtils.SPACE, UNDERSCORE);
        return String.format(FILENAME_FORMATTER, caseNumber.toLowerCase().replaceAll(StringUtils.SPACE, UNDERSCORE),
                caseName.toLowerCase()).replaceAll(StringUtils.SPACE, UNDERSCORE);
    }
}
