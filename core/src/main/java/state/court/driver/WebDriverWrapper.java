package state.court.driver;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.springframework.core.io.ClassPathResource;

import java.io.Closeable;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author foxy
 * @since 05.09.18.
 */
public class WebDriverWrapper implements Closeable {

    private static final String DRIVER_PROPERTY_NAME = "webdriver.chrome.driver";
    private static final String CHROME_DRIVER_PATH = "core/src/main/resources/driver/chromedriver";

    private final Logger logger = getLogger(WebDriverWrapper.class);

    private WebDriver webDriver;

    public WebDriverWrapper() {
        if (System.getProperty(DRIVER_PROPERTY_NAME) == null)
            System.setProperty(DRIVER_PROPERTY_NAME, new ClassPathResource(CHROME_DRIVER_PATH).getPath());
        final ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1400,1000");
        options.addArguments("--disable-plugin",
                "--disable-notifications",
                "--ignore-certificate-errors",
                "--headless",
                "--disable-dev-shm-usage",
                "--no-sandbox");
        webDriver = new ChromeDriver(options);
    }

    public void close() {
        try {
            webDriver.close();
        } catch (WebDriverException ex) {
            webDriver.quit();
        }
    }

    public String getPageSource() {
        return webDriver.getPageSource();
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public void get(String url) {
        webDriver.get(url);
    }

    public WebElement findElement(By var1) {
        return webDriver.findElement(var1);
    }

    public List<WebElement> findElements(By var1) {
        return webDriver.findElements(var1);
    }

    public boolean exists(By by) {
        return !webDriver.findElements(by).isEmpty();
    }

    public void delay(int min, int max) {
        long delayValue = (long) (min + Math.random() * (max - min));
        try {
            Thread.sleep(delayValue);
        } catch (InterruptedException e) {
            logger.error("Interrupted!", e);
            Thread.currentThread().interrupt();
        }
    }

    public void sendKeys(By by, String keysToSend) {
        WebElement element = this.webDriver.findElement(by);
        for (char value : keysToSend.toCharArray()) {
            element.sendKeys(Character.toString(value));
            delay(200, 2000);
        }
    }
}
