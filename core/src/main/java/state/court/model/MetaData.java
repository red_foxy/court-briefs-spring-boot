package state.court.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author foxy
 * @since 23.11.18.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MetaData implements Serializable {

    @JsonProperty("filename")
    private String fileName;

    @JsonProperty("url")
    private String appellateBriefUrl;

    @JsonProperty("date")
    private String briefField;

    private String retrieved;
    private String jurisdiction;

    @JsonProperty("docket")
    private String docketOrCaseNumber;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("case-name")
    private String caseName;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String description;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("plaintiff-emails")
    private String[] plaintiffEmails = {};

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("defendant-emails")
    private String[] defendantEmails = {};

    @JsonIgnore
    private String caseNumberUrl;
}
