package state.court.constant;

/**
 * @author foxy
 * @since 22.11.18.
 */
public class Constants {
    public static final String PDF_SUFFIX = ".pdf";
    public static final String JSON_EXTENSION = ".json";
    public static final String ZIP_SUFFIX = ".zip";
    public static final String TAR_GZ_SUFFIX = ".tar.gz";
    public static final String SCRIPT_SUBDIRECTORY_NAME = "valid";
    public static final String TAR_SUFFIX = ".tar.gz";
    public static final String LINK_ATTR_SELECTOR = "href";
    public static final String LINK = "a";
    public static final String USER_DIRECTORY = System.getProperty("user.dir");
    public static final String LINK_SELECTOR = "a[href]";
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String FILENAME_FORMATTER_NUMBER = "%s_%s_%d";
    public static final String FILENAME_FORMATTER = "%s_%s";
    public static final String UNDERSCORE = "_";
    public static final String CHAR_FOR_REPLACE = "/";
    public static final String PATH_BRIEF_SCHEMA = USER_DIRECTORY + "/brief-schemas/brief-schemas.json";
    public static final String DATE_PATTERN_LIST = "[M/d/yyyy][M/dd/yyyy][M/d/yy][M/dd/yy][MM/d/yy][MM/dd/yy][MM/d/yyyy][MM/dd/yyyy][yyyy-MM-dd]";
    public static final String SPACE_ENCODE = "%20";
    public static final String PROXY_HOST = "35.238.83.226";
    public static final Integer PROXY_PORT = 8888;

    private Constants() {
        throw new IllegalStateException("Utility class");
    }
}
