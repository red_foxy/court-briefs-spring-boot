package state.court.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import state.court.model.MetaData;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * @author foxy
 * @since 28.03.19.
 */
public class JsonValidator {
    private static final ObjectMapper mapper = new ObjectMapper();

    static String convertMetadataToJson(MetaData metadata) throws IOException {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(metadata);
    }

    public static void validationSchema(String recourseFilePath, String jsonString) throws IOException {
        Schema jsonSchema = getJsonSchemaFromFile(recourseFilePath);
        JSONObject jsonObject = new JSONObject(jsonString);
        jsonSchema.validate(jsonObject);
    }

    private static Schema getJsonSchemaFromFile(String filePath) throws IOException {
        try (Reader reader = new FileReader(filePath)) {
            JSONObject rawSchema = new JSONObject(new JSONTokener(reader));
            return SchemaLoader.load(rawSchema);
        }
    }

    private JsonValidator() {
        throw new IllegalStateException("Utility class");
    }
}
