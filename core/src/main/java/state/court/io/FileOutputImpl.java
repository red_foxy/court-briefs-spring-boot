package state.court.io;


import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.everit.json.schema.ValidationException;
import org.slf4j.Logger;
import state.court.entity.FileNames;
import state.court.entity.ScraperInfo;
import state.court.entity.ValidationResult;
import state.court.model.MetaData;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

import static org.slf4j.LoggerFactory.getLogger;
import static state.court.constant.Constants.*;
import static state.court.io.TarApacheCompressUtil.compressFiles;

@Slf4j
public class FileOutputImpl implements FileOutput {

    private static final String PATH_PART = "data/";
    private static final String DATA_PATH = USER_DIRECTORY + File.separator + PATH_PART;

    private final Logger logger = getLogger(FileOutputImpl.class);

    private final String subdirName;

    public FileOutputImpl(String subdirName) {
        this.subdirName = DATA_PATH + subdirName;
        prepareDir(this.subdirName);
    }

    @Override
    public void writeToFile(InputStream inputStream, String fileName, String extension) {

        File file = new File(subdirName.concat(File.separator).concat(fileName).concat(extension));
        if (!file.exists()) {
            try {
                Files.copy(inputStream, Paths.get(String.valueOf(file)), StandardCopyOption.REPLACE_EXISTING);
                inputStream.close();
                logger.info("Saved in file - {}", file.getName());
            } catch (Exception e) {
                logger.error("Error while writing file: ".concat(file.getName()), e);
            }
        } else {
            logger.info("File already exist - {}", file.getName());
        }
    }

    private void getFileBytesByProxy(String host, int port, String url, String fileName) {
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port));
        URL documentUrl;
        try {
            documentUrl = new URL(url.replaceAll("\\s", SPACE_ENCODE));
            HttpURLConnection httpUrlConnection = (HttpURLConnection) documentUrl.openConnection(proxy);
            httpUrlConnection.setConnectTimeout(6000000);
            httpUrlConnection.connect();
            try (InputStream inputStream = httpUrlConnection.getInputStream()) {
                writeToFile(inputStream, fileName, PDF_SUFFIX);
            }
        } catch (Exception e) {
            log.error("getFileBytesByProxy {}", e.getMessage());
        }
    }

    @Override
    public void prepareDir(String dirName) {
        File dir = new File(dirName);
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    public List<String> downloadPDF(MetaData metaData, String subdirName, List<String> existedFiles, ScraperInfo scraperInfo) throws IOException {
        Set<String> fileNames = new HashSet<>();
        String fileName = getCaseBriefFileName(metaData);
        try {
            if (metaData.getAppellateBriefUrl().trim().contains("http")) {
                URL downloadedFileUrl = new URL(metaData.getAppellateBriefUrl().trim());
                HttpURLConnection connection = (HttpURLConnection) downloadedFileUrl.openConnection();
                int code = connection.getResponseCode();
                if (code >= HttpStatus.SC_FORBIDDEN) {
                    logger.info("File not find by url {}", downloadedFileUrl);
                    return new ArrayList<>(fileNames);
                }
                InputStream inputStream = connection.getInputStream();
                boolean fileExist = ifFileExist(existedFiles, fileName);
                if (!fileExist) {
                    String metaDataAsString = JsonValidator.convertMetadataToJson(metaData);
                    boolean isValid = isMetaDataValid(scraperInfo, metaDataAsString);
                    if (isValid) {
                        writeMetaDataToJson(metaDataAsString, fileName, subdirName);
                        writeToFile(inputStream, fileName, PDF_SUFFIX);
                        fileNames.add(fileName);
                    }
                } else {
                    logger.info("File already exists - {}", fileName);
                }
            }
        } catch (MalformedURLException ex) {
            logger.info(metaData.getCaseName());
        }
        return new ArrayList<>(fileNames);
    }

    public List<String> downloadPDFWithProxy(MetaData metaData, String subdirName, List<String> existedFiles,
                                             ScraperInfo scraperInfo, String host, int port) throws IOException {
        Set<String> fileNames = new HashSet<>();
        String url = metaData.getAppellateBriefUrl().trim();
        String fileName = getCaseBriefFileName(metaData);
        try {
            if (url.contains("http")) {
                boolean fileExist = ifFileExist(existedFiles, fileName);
                if (!fileExist) {
                    String metaDataAsString = JsonValidator.convertMetadataToJson(metaData);
                    boolean isValid = isMetaDataValid(scraperInfo, metaDataAsString);
                    if (isValid) {
                        writeMetaDataToJson(metaDataAsString, fileName, subdirName);
                        getFileBytesByProxy(host, port, url, fileName);
                        fileNames.add(fileName);
                    }
                } else {
                    logger.info("File already exists - {}", fileName);
                }
            }
        } catch (MalformedURLException ex) {
            logger.info(metaData.getCaseName());
        }
        return new ArrayList<>(fileNames);
    }

    private String getCaseBriefFileName(MetaData metaData) {
        String fileName = metaData.getFileName().replaceAll(CHAR_FOR_REPLACE, UNDERSCORE);
        int symbolCountFileName = fileName.length();
        if (symbolCountFileName >= 234) {
            fileName = StringUtils.substring(fileName, 0, 234);
        }
        return fileName;
    }

    private boolean isMetaDataValid(ScraperInfo scraperInfo, String metaDataAsString) throws IOException {
        try {
            JsonValidator.validationSchema(PATH_BRIEF_SCHEMA, metaDataAsString);
            return true;
        } catch (ValidationException validException) {
            ValidationResult result = new ValidationResult();
            result.setFile(metaDataAsString);
            result.setValidationException(validException.getMessage());
            result.setScraperInfo(scraperInfo);
            return false;
        }
    }

    private void writeMetaDataToJson(String normalView, String fileNamePart, String subdirName) throws IOException {
        String fileName = fileNamePart.concat(JSON_EXTENSION);
        if (fileName.isEmpty()) {
            return;
        }
        Path path = Paths.get(PATH_PART + subdirName);
        if (path.toFile().exists()) {
            Files.createDirectories(path);
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path + File.separator + fileName))) {
            writer.write(normalView);
        } catch (JsonProcessingException e) {
            logger.error("Could not create JSON file!");
        }
    }

    public void tarIt(String downloadDirPath) {
        try {
            List<File> compressedFiles = compressFiles(downloadDirPath);
            removeFiles(compressedFiles);
        } catch (IOException e) {
            log.error("Files are not compressed");
        }
    }

    private boolean ifFileExist(List<String> existingFileNames, String fileName) {
        return !existingFileNames.isEmpty() && existingFileNames.contains(fileName);
    }

    private void removeFiles(List<File> fileList) {
        for (File file : fileList) {
            try {
                if (Files.deleteIfExists(file.toPath())) {
                    log.info("Remove file - {}", file);
                }
            } catch (IOException e) {
                log.error("File doesn't exist!", e);
            }
        }
    }

    public List<String> getExistFiles(List<FileNames> fileNames) {
        List<String> existingFiles = new ArrayList<>();
        for (FileNames file : fileNames) {
            existingFiles.add(file.getFileName());
        }
        return existingFiles;
    }
}
