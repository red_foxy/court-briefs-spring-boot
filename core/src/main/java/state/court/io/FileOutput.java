package state.court.io;

import java.io.InputStream;

public interface FileOutput {

    void writeToFile(InputStream inputStream, String fileName, String extension);

    void prepareDir(String dirName);
}