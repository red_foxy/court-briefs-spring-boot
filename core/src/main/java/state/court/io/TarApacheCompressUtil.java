package state.court.io;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static state.court.constant.Constants.*;

/**
 * @author foxy
 * @since 16.04.19.
 */
@Log4j2
public class TarApacheCompressUtil {

    private TarApacheCompressUtil() {
    }

    public static List<File> compressFiles(String sourceFolder) throws IOException {
        List<File> fileList = getFileList(sourceFolder);
        if (fileList.isEmpty()) {
            log.info("There are not files for compression");
        } else {
            String outputTarFolder = sourceFolder
                    + File.separator
                    + LocalDate.now()
                    + TAR_GZ_SUFFIX;
            try (TarArchiveOutputStream tarOut = getTarArchiveOutputStream(outputTarFolder)) {
                for (File file : fileList) {
                    addToArchiveCompression(tarOut, file, LocalDate.now().toString());
                }
            }
        }
        return fileList;
    }

    private static TarArchiveOutputStream getTarArchiveOutputStream(String name) throws IOException {
        TarArchiveOutputStream compressOutputStream =
                new TarArchiveOutputStream(new GzipCompressorOutputStream(new FileOutputStream(name)));
        compressOutputStream.
                setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
        compressOutputStream.
                setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
        compressOutputStream.
                setAddPaxHeadersForNonAsciiNames(true);
        return compressOutputStream;
    }

    private static void addToArchiveCompression(TarArchiveOutputStream out, File file, String dir) throws IOException {
        String entry = dir + File.separator + file.getName();
        if (file.isFile()) {
            out.putArchiveEntry(new TarArchiveEntry(file, entry));
            try (FileInputStream in = new FileInputStream(file)) {
                IOUtils.copy(in, out);
            }
            out.closeArchiveEntry();
        } else {
            log.info("{} is bad file format", file.getName());
        }
    }

    private static List<File> getFileList(String sourceFolder) {
        File[] files = new File(sourceFolder).listFiles();
        if (files == null) {
            return Collections.emptyList();
        }
        List<File> fileList = new ArrayList<>();
        Arrays.stream(files)
                .filter(TarApacheCompressUtil::isCompressedFiles)
                .forEach(fileList::add);
        return fileList;
    }

    private static boolean isCompressedFiles(File file) {
        String fileName = file.getName();
        if (fileName.contains(ZIP_SUFFIX)) {
            return false;
        }
        if (fileName.contains(TAR_GZ_SUFFIX)) {
            return false;
        }
        return !fileName.contains(SCRIPT_SUBDIRECTORY_NAME);
    }
}
