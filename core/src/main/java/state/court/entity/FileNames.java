package state.court.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author foxy
 * @since 31.01.19.
 */
@Data
@Entity(name = "file_name_list")
@ToString(exclude = {"scraperInfo"})
public class FileNames implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "file_name")
    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "scraper_id")
    private ScraperInfo scraperInfo;
}
