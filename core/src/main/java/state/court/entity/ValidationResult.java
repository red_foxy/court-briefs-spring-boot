package state.court.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author foxy
 * @since 27.03.19.
 */
@Data
@Entity(name = "metadata_validation_results")
@ToString(exclude = {"scraperInfo"})
public class ValidationResult implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "metadata_file_json", length=512)
    private String file;

    @Column(name = "validation_exception")
    private String validationException;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "scraper_id")
    private ScraperInfo scraperInfo;
}
