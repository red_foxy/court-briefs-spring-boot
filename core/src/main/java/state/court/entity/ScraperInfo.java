package state.court.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author foxy
 * @since 16.01.19.
 */
@Data
@Entity(name = "scraper_start_info")
@ToString(exclude = {"fileNameList"})
public class ScraperInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "scrapper_name")
    private String scraperName;

    @Column(name = "date_scrapper_start")
    private Date startDate;

    @OneToMany(mappedBy = "scraperInfo", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FileNames> fileNameList = new ArrayList<>();

    @OneToMany(mappedBy = "scraperInfo", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ValidationResult> validationResultList = new ArrayList<>();
}
