package state.court;

import org.apache.commons.lang3.StringUtils;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import state.court.driver.WebDriverWrapper;
import state.court.io.JsonValidator;
import state.court.io.TarApacheCompressUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static state.court.constant.Constants.PATH_BRIEF_SCHEMA;

/**
 * @author foxy
 * @since 27.03.19.
 */
public class CoreTest {

    private static final String PATH_USER_DIR = System.getProperty("user.dir");
    private static final String PATH_NON_REQUIRE_FIELD_JSON = PATH_USER_DIR + "/src/test/resources/VALID-s046848_people_v._dalton_(kerry_lyn)_1.json";
    private static final String PATH_EMPTY_VALUE_JSON = PATH_USER_DIR + "/src/test/resources/EMPTY-VALUE-s046848_people_v._dalton_(kerry_lyn)_2.json";
    private static final String ERROR_NON_REQ_FIELD = "#: required key [filename] not found";

    @Test
    public void validationTest() throws IOException {
        try (InputStream inputStream = new FileInputStream(PATH_BRIEF_SCHEMA)) {
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            Schema schema = SchemaLoader.load(rawSchema);
            String content = new String(Files.readAllBytes(Paths.get(PATH_NON_REQUIRE_FIELD_JSON)));
            try {
                schema.validate(new JSONObject(content));
            } catch (ValidationException validException) {
                assertEquals("Test if failed", ERROR_NON_REQ_FIELD, validException.getMessage());
            }
        }
    }

    @Test
    public void schemaFromFileNonRequireField() throws IOException {
        try {
            JsonValidator.validationSchema(PATH_BRIEF_SCHEMA, getJsonStringFromFile(PATH_NON_REQUIRE_FIELD_JSON));
        } catch (ValidationException validException) {
            assertEquals("Test if failed", ERROR_NON_REQ_FIELD, validException.getMessage());
        }
    }

    @Test
    public void schemaFromFileEmptyValue() throws IOException {
        try {
            JsonValidator.validationSchema(PATH_BRIEF_SCHEMA, getJsonStringFromFile(PATH_EMPTY_VALUE_JSON));
        } catch (ValidationException validException) {
            assertEquals("Test if failed", ERROR_NON_REQ_FIELD, validException.getMessage());
        }
    }

    private String getJsonStringFromFile(String filePath) throws IOException {
        return new String(Files.readAllBytes(Paths.get(filePath)));
    }

    @Test
    public void fileNameTest() {
        String str = "122973,122985,122986,122987,122988,122989,122992,122993,122994,122996,122997,122998,122999,123000," +
                "123001,_123002,123003,123004,123005,123006,123007,123008,123009,123011,123012,123013,123014,123015," +
                "123016,_123017,123018,123019,123020,123021_cons._ameren_transmission_company_of_illinois," +
                "_appellantv._richard_l._hutchings.json";
        int symbolCountFileName = str.length();
        if (symbolCountFileName >= 249) {
            str = StringUtils.substring(str, 0, 249);
            str = str + ".json";
        }
        System.out.println(str);

    }

    @Test
    public void compressTest() throws IOException {
        String subDir = "/home/foxy/Projects/state-court/data/massachusetts";
        TarApacheCompressUtil.compressFiles(subDir);
    }

    @Test
    public void findMeInGoogleTest() {
//        sudo apt-get purge google-chrome-stable
//        rm ~/.config/google-chrome/ -rf
//        23
        long startTime = System.nanoTime();
        WebDriverWrapper webDriver = new WebDriverWrapper();
        webDriver.get("https://searx.site/");
        webDriver.delay(1000, 1500);
        webDriver.sendKeys(By.cssSelector("#q"), "Анастасия Погановская linkedIn");
        webDriver.sendKeys(By.cssSelector("#q"), String.valueOf(Keys.ENTER));
        webDriver.delay(1000, 1500);
        webDriver.findElement(By.cssSelector("#main_results > div:nth-child(2) > h4 > a")).sendKeys(Keys.chord(Keys.CONTROL, Keys.RETURN));
        ArrayList<String> tabs = new ArrayList<>(webDriver.getWebDriver().getWindowHandles());
        webDriver.getWebDriver().switchTo().window(tabs.get(1));
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println(duration);
    }
}
