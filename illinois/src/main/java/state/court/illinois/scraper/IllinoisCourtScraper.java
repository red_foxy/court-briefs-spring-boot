package state.court.illinois.scraper;


import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import state.court.driver.WebDriverWrapper;
import state.court.entity.FileNames;
import state.court.entity.ScraperInfo;
import state.court.illinois.parser.IllinoisCourtParser;
import state.court.io.FileOutputImpl;
import state.court.model.MetaData;
import state.court.repository.FileNameRepository;
import state.court.repository.ScraperRepository;
import state.court.utils.ParserUtils;
import state.court.utils.ZipUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static state.court.illinois.constant.IllinoisCourtConstants.*;

/**
 * @author foxy
 * @since 12.12.18.
 */
@Slf4j
@Component
public class IllinoisCourtScraper {

    private final ScraperRepository scraperRepository;
    private final FileNameRepository fileNameRepository;
    private final IllinoisCourtParser illinoisParser;

    @Autowired
    public IllinoisCourtScraper(ScraperRepository scraperRepository,
                                FileNameRepository fileNameRepository,
                                IllinoisCourtParser illinoisParser) {
        this.scraperRepository = scraperRepository;
        this.fileNameRepository = fileNameRepository;
        this.illinoisParser = illinoisParser;
    }

    public void scrapeCourt() throws IOException {
        ScraperInfo scraperInfo = scraperRepository.save(ParserUtils.saveScraperInfo(SUB_DIR_NAME));
        try (final WebDriverWrapper webDriver = new WebDriverWrapper()) {
            webDriver.get(MAIN_URL);
            collectUrlAndScrape(webDriver, scraperInfo);
        }
        ZipUtils.archiveFiles(SUB_DIR_NAME, SUB_DIR_NAME_UPDATE);
    }

    private void collectUrlAndScrape(WebDriverWrapper webDriver, ScraperInfo scraperInfo) throws IOException {
        final List<String> yearUrlList = new ArrayList<>();
        getUrlListFromSelector(yearUrlList, webDriver, scraperInfo);
        parsePageByUrl(yearUrlList, webDriver, scraperInfo);
    }

    private void getUrlListFromSelector(List<String> yearUrlList, WebDriverWrapper webDriver, ScraperInfo scraperInfo) {
        String currentYear = ParserUtils.getCurrentYear();
        String previousYear = ParserUtils.getPreviousYear();
        final List<String> partOfUrlList = new ArrayList<>();
        Select dropdown = new Select(webDriver.findElement(By.cssSelector(SELECTOR_DROPDOWN)));
        List<WebElement> options = dropdown.getOptions();
        for (WebElement option : options) {
            String partOfUrl = option.getAttribute(DROPDOWN_OPTION);
            if (!getExistFiles(scraperInfo).isEmpty()) {
                if (partOfUrl.contains(currentYear) || partOfUrl.contains(previousYear)) {
                    partOfUrlList.add(partOfUrl);
                }
            } else {
                partOfUrlList.add(partOfUrl);
            }
        }
        prepareFullUrlList(yearUrlList, partOfUrlList);
    }

    private void prepareFullUrlList(List<String> yearUrlList, List<String> partOfUrlList) {
        for (String partOfUrl : partOfUrlList) {
            if (partOfUrl.contains(SHARP))
                continue;

            String fullUrl = String.format(URL_FORMAT, MAIN_URL_PART, partOfUrl);
            yearUrlList.add(fullUrl);
        }
    }

    private void parsePageByUrl(List<String> yearUrlList, WebDriverWrapper webDriver, ScraperInfo scraperInfo) throws IOException {
        for (String yearUrl : yearUrlList) {
            webDriver.get(yearUrl);
            parseCaseNumberPage(webDriver, scraperInfo);
        }
    }

    private void parseCaseNumberPage(WebDriverWrapper webDriver, ScraperInfo scraperInfo) throws IOException {
        String pageSource = webDriver.getPageSource();
        final List<MetaData> metaDataList = illinoisParser.parsePage(pageSource, webDriver);
        List<String> fileNames = new ArrayList<>();
        for (MetaData metaData : metaDataList) {
            final List<String> downloadPDFList = downloadPDF(metaData, scraperInfo);
            if (!downloadPDFList.isEmpty())
                fileNames.addAll(downloadPDFList);
        }
        log.info("Saving process finished!");
        ParserUtils.setFileNameToScraperInfo(fileNames, scraperInfo);
        scraperRepository.save(scraperInfo);
    }

    private List<String> downloadPDF(MetaData metaData, ScraperInfo scraperInfo) throws IOException {
        FileOutputImpl output = new FileOutputImpl(SUB_DIR_NAME);
        List<String> existedFiles = getExistFiles(scraperInfo);
        return output.downloadPDF(metaData, SUB_DIR_NAME, existedFiles, scraperInfo);
    }

    private List<String> getExistFiles(ScraperInfo scraperInfo) {
        FileOutputImpl output = new FileOutputImpl(SUB_DIR_NAME);
        List<FileNames> fileNames = fileNameRepository.findAllByScraperInfoScraperName(scraperInfo.getScraperName());
        return output.getExistFiles(fileNames);
    }
}
