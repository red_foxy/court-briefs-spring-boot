package state.court.illinois.constant;

/**
 * @author foxy
 * @since 28.03.19.
 */
public class IllinoisCourtConstants {

    public static final String MAIN_URL = "http://illinoiscourts.gov/SupremeCourt/Docket/default.asp";
    public static final String SUB_DIR_NAME = "illinois";
    public static final String SUB_DIR_NAME_UPDATE = "/data/" + SUB_DIR_NAME;
    public static final String SELECTOR_DROPDOWN = "#SelectDocket";
    public static final String DROPDOWN_OPTION = "value";
    public static final String SHARP = "#";
    public static final String MAIN_URL_PART = "http://illinoiscourts.gov";
    public static final String PDF_URL_PART = "http://illinoiscourts.gov/SupremeCourt/Docket/";
    public static final String URL_FORMAT = "%s%s";
    public static final String BRIEF_FIELD_PATTERN = "\\d*/\\d*/\\d{2}";
    public static final String CASE_COURT_TABLE_SELECTOR = "body > table.body2 > tbody > tr > td.center > table.content > tbody > tr > td > div.container > div > table";
    public static final String CASE_COURT_TABLE_SELECTOR_SECOND_TYPE = "body > table.body2 > tbody > tr > td.center > table.content > tbody > tr > td > table > tbody > tr > td > table";
    public static final String CASE_COURT_TABLE_SELECTOR_THIRD_TYPE = "body > table.body2 > tbody > tr > td.center > table.content > tbody > tr > td > div > table";
    public static final String CASE_COURT_TABLE_HEADER = "tbody > tr:nth-child(1) > td";
    public static final String CASE_NUMBER_PART = "Case No";
    public static final String DOT_CHAR = ".";
    public static final String DASH_CHAR = "-";
    public static final String DESCRIPTION_ROW_SELECTOR = "tbody > tr > td:nth-child(2)";
    public static final String JURISDICTION = "Illinois Supreme Court";
    public static final String DESCRIPTION = "Description";

    private IllinoisCourtConstants() {
        throw new IllegalStateException("Utility class");
    }
}
