package state.court.illinois.parser;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import state.court.driver.WebDriverWrapper;
import state.court.model.MetaData;
import state.court.utils.ParserUtils;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static state.court.constant.Constants.*;
import static state.court.illinois.constant.IllinoisCourtConstants.*;

/**
 * @author foxy
 * @since 29.03.19.
 */
@Slf4j
@Component
public class IllinoisCourtParser {

    public List<MetaData> parsePage(String page, WebDriverWrapper webDriver) {
        List<MetaData> metaDataList = new ArrayList<>();
        Document document = Jsoup.parse(page);
        Elements caseCourtTableElements = document.select(CASE_COURT_TABLE_SELECTOR);
        if (caseCourtTableElements.isEmpty()) {
            caseCourtTableElements = document.select(CASE_COURT_TABLE_SELECTOR_SECOND_TYPE);
        }
        if (caseCourtTableElements.isEmpty()) {
            caseCourtTableElements = document.select(CASE_COURT_TABLE_SELECTOR_THIRD_TYPE);
        }
        for (Element tableElement : caseCourtTableElements) {
            metaDataList.addAll(parseCaseNumberParagraph(tableElement, webDriver));
        }
        return metaDataList;
    }

    private String getCaseName(Element tableElement) {
        String dirtyCaseName = tableElement.selectFirst(CASE_COURT_TABLE_HEADER).text();
        String caseName = dirtyCaseName;
        if (dirtyCaseName.contains(CASE_NUMBER_PART)) {
            caseName = StringUtils.substringAfter(dirtyCaseName, DASH_CHAR).trim();
        }
        return caseName;
    }

    private String getCaseNumber(Element tableElement) {
        String dirtyCaseName = tableElement.selectFirst(CASE_COURT_TABLE_HEADER).text();
        String caseNumber = dirtyCaseName;
        if (dirtyCaseName.contains(CASE_NUMBER_PART)) {
            caseNumber = StringUtils.substringBetween(dirtyCaseName, DOT_CHAR, DASH_CHAR).trim();
        }
        return caseNumber;
    }

    private List<MetaData> parseCaseNumberParagraph(Element caseParagraphElement, WebDriverWrapper webDriver) {
        String caseName = getCaseName(caseParagraphElement);
        String caseNumber = getCaseNumber(caseParagraphElement);
        List<MetaData> metaDataList = new ArrayList<>();
        final Elements caseCourtSecondColumnElements = caseParagraphElement.select(DESCRIPTION_ROW_SELECTOR);
        List<String> appellateBriefUrlList = parseAppellateBriefUrl(caseCourtSecondColumnElements, webDriver);
        List<String> briefDescriptionList = parseDescription(caseCourtSecondColumnElements);
        List<String> briefFieldList = getString(caseParagraphElement.text());
        if (appellateBriefUrlList.size() == briefFieldList.size()) {
            for (int i = 0; i < appellateBriefUrlList.size(); i++) {
                MetaData metaData = new MetaData();
                metaData.setFileName(ParserUtils.getFilename(caseNumber, caseName, i));
                metaData.setJurisdiction(JURISDICTION);
                metaData.setCaseName(caseName);
                metaData.setDocketOrCaseNumber(caseNumber);
                metaData.setBriefField(ParserUtils.formatBriefField(briefFieldList.get(i)));
                metaData.setRetrieved(LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_PATTERN)));
                metaData.setDescription(briefDescriptionList.get(i));
                metaData.setAppellateBriefUrl(appellateBriefUrlList.get(i));
                metaDataList.add(metaData);
            }
        }
        return metaDataList;
    }

    private List<String> getString(String text) {
        final List<String> briefFieldList = new ArrayList<>();
        Pattern pattern = Pattern.compile(BRIEF_FIELD_PATTERN);
        return ParserUtils.getStrings(text, briefFieldList, pattern);
    }

    private List<String> parseDescription(Elements caseCourtSecondColumnElements) {
        List<String> briefDescriptionList = new ArrayList<>();
        for (Element descriptionElement : caseCourtSecondColumnElements) {
            String description = descriptionElement.text();
            if ((description.contains(DESCRIPTION)) || (description.equals(StringUtils.EMPTY)))
                continue;
            briefDescriptionList.add(description);
        }
        return briefDescriptionList;
    }

    private List<String> parseAppellateBriefUrl(Elements caseCourtSecondColumnElements, WebDriverWrapper webDriver) {
        String currentYear = ParserUtils.getCurrentYear();
        String currentUrl = webDriver.getWebDriver().getCurrentUrl();
        final String urlFormat = StringUtils.substringBeforeLast(currentUrl, CHAR_FOR_REPLACE);
        List<String> appellateBriefUrlList = new ArrayList<>();
        for (Element appellateBriefUrlElement : caseCourtSecondColumnElements) {
            try {
                String appellateBriefUrl = appellateBriefUrlElement.select(LINK).attr(LINK_ATTR_SELECTOR);
                if (appellateBriefUrl.contains(currentYear)) {
                    appellateBriefUrlList.add(preparePdfFullUrl(appellateBriefUrl));
                } else if (!appellateBriefUrl.equals(StringUtils.EMPTY)) {
                    String appellateUrl = urlFormat + File.separator + appellateBriefUrl;
                    appellateBriefUrlList.add(appellateUrl);
                }
            } catch (Exception ex) {
                log.info("Selector not found");
            }
        }
        return appellateBriefUrlList;
    }

    private String preparePdfFullUrl(String partOfUrl) {
        return String.format(URL_FORMAT, PDF_URL_PART, partOfUrl);
    }
}
