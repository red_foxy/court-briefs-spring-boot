package state.court.illinois.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import state.court.service.CourtService;
import state.court.illinois.scraper.IllinoisCourtScraper;

import javax.transaction.Transactional;
import java.io.IOException;

/**
 * @author foxy
 * @since 12.12.18.
 */
@Service
public class IllinoisCourtServiceImpl implements CourtService {

    private final IllinoisCourtScraper illinoisScraper;

    @Autowired
    public IllinoisCourtServiceImpl(IllinoisCourtScraper illinoisScraper) {
        this.illinoisScraper = illinoisScraper;
    }

    @Override
    @Transactional
    public void scrapeCourt() throws IOException {
        illinoisScraper.scrapeCourt();
    }
}
