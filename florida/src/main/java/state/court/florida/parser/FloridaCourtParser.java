package state.court.florida.parser;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;
import state.court.driver.ProxyWebDriverWrapper;
import state.court.model.MetaData;
import state.court.utils.ParserUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static state.court.constant.Constants.*;
import static state.court.florida.constant.FloridaCourtConstants.*;

/**
 * @author foxy
 * @since 18.03.19.
 */
@Component
@Slf4j
public class FloridaCourtParser {

    public List<MetaData> parseCaseNumberPage(List<String> yearSecondLevelUrlList) {
        List<MetaData> metaDataList = new ArrayList<>();
        final ProxyWebDriverWrapper webDriver = new ProxyWebDriverWrapper();
        for (String yearSecondLevelUrl : yearSecondLevelUrlList) {
            webDriver.get(yearSecondLevelUrl);
            String pageSource = webDriver.getPageSource();
            metaDataList.addAll(parsePage(pageSource, webDriver));
        }
        webDriver.close();
        return metaDataList;
    }

    private List<MetaData> parseMetaDataParagraph(Element caseParagraphElement, Elements paragraphUrlList,
                                                  WebElement paragraphWebElement) {
        final List<String> briefFieldList;
        final List<String> briefDescriptionList;
        final List<MetaData> metaDataList = new ArrayList<>();
        final List<String> appellateBriefUrlList = new ArrayList<>();
        final Set<String> uniqUrlSet = new HashSet<>();

        String caseNumber = getCaseNumber(caseParagraphElement);
        String caseName = getCaseName(caseParagraphElement, caseNumber);

        parseAppellateBriefUrlList(paragraphUrlList, appellateBriefUrlList, uniqUrlSet);
        String fullTextParagraphDescription = paragraphWebElement.getText();
        final String fullTextParagraph = caseParagraphElement.text();
        briefFieldList = getString(fullTextParagraph, appellateBriefUrlList.size());
        briefDescriptionList = parseDescription(fullTextParagraphDescription, caseNumber, caseName);

        for (int i = 0; i < appellateBriefUrlList.size(); i++) {
            MetaData metaData = new MetaData();
            metaData.setFileName(ParserUtils.getFilename(caseNumber, caseName, i));
            metaData.setAppellateBriefUrl(appellateBriefUrlList.get(i));
            if (!briefFieldList.isEmpty())
                metaData.setBriefField(ParserUtils.formatBriefField(briefFieldList.get(i)));
            metaData.setRetrieved(LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_PATTERN)));
            metaData.setJurisdiction(FLORIDA_JURISDICTION);
            metaData.setDocketOrCaseNumber(caseNumber);
            metaData.setCaseName(caseName);
            if (!briefDescriptionList.isEmpty()) {
                if (briefDescriptionList.size() != appellateBriefUrlList.size()) {
                    briefDescriptionList.add(StringUtils.EMPTY);
                }
                metaData.setDescription(briefDescriptionList.get(i));
            }
            metaDataList.add(metaData);
        }
        return metaDataList;
    }

    private void parseAppellateBriefUrlList(Elements paragraphUrlList, List<String> appellateBriefUrlList,
                                            Set<String> uniqUrlSet) {
        for (Element element : paragraphUrlList) {
            String urlCut = element.attr(LINK_ATTR_SELECTOR);
            if (StringUtils.containsIgnoreCase(urlCut, BAD_URL_PART))
                urlCut = StringUtils.substringBeforeLast(urlCut, SHARP);
            uniqUrlSet.add(urlCut);
        }
        if (!uniqUrlSet.isEmpty()) {
            appellateBriefUrlList.addAll(uniqUrlSet);
        }
    }

    private List<String> getString(String text, int countGeneralLink) {
        final List<String> briefFieldList = new ArrayList<>();
        Pattern pattern = Pattern.compile(DATE_REGULAR_EXPRESSION_PATTERN);
        return ParserUtils.getStrings(text, briefFieldList, pattern, countGeneralLink);
    }

    private List<String> parseDescription(String fullTextParagraph, String caseNumber, String caseName) {
        List<String> briefDescriptionList = new ArrayList<>();
        if (fullTextParagraph.contains(StringUtils.LF)) {
            String[] split = fullTextParagraph.split(StringUtils.LF);
            for (String partOfParagraph : split) {
                String description = StringUtils.EMPTY;
                String[] splitDash = partOfParagraph.split(DASH_WITH_TWO_SPACES);
                if (splitDash.length == 3) {
                    description = splitDash[1];
                    briefDescriptionList.add(description.trim());
                } else {
                    parseBadFormattedDescription(caseNumber, caseName, briefDescriptionList, partOfParagraph, description);
                }
            }
        } else if (fullTextParagraph.contains(CASE_NAME_END_STR)) {
            String description = StringUtils.substringBetween(fullTextParagraph, CASE_NAME_END_STR, CASE_NAME_END_STR);
            if (StringUtils.isNotBlank(description))
                briefDescriptionList.add(description.trim());
        }
        return briefDescriptionList;
    }

    private void parseBadFormattedDescription(String caseNumber, String caseName, List<String> briefDescriptionList,
                                              String partOfParagraph, String description) {
        if (partOfParagraph.contains(DATE_SEPARATOR)) {
            description = StringUtils.substringBefore(partOfParagraph, DATE_SEPARATOR)
                    .replaceAll(REGEX_CLEAR_DESCRIPTION, StringUtils.EMPTY);
        } else if (partOfParagraph.contains(CASE_NAME_END_STR)) {
            if (partOfParagraph.contains(LARGE_DASH_CHAR)) {
                description = StringUtils.substringBetween(partOfParagraph, CASE_NAME_END_STR, LARGE_DASH_CHAR);
            } else {
                description = StringUtils.substringBeforeLast(partOfParagraph, CASE_NAME_END_STR);
            }
        } else if (!partOfParagraph.contains(CASE_NAME_END_STR) && partOfParagraph.contains(LARGE_DASH_CHAR)) {
            description = StringUtils.substringBefore(partOfParagraph, LARGE_DASH_CHAR);
        } else if (partOfParagraph.contains(DESCRIPTION_PART_BRIEF) || partOfParagraph.contains(DESCRIPTION_PART_LIST)) {
            description = partOfParagraph;
        }
        if (StringUtils.isNotBlank(description) || StringUtils.isNotEmpty(description)) {
            if (description.trim().equals(caseNumber.concat(StringUtils.SPACE).concat(caseName))) {
                return;
            }
            if (description.endsWith("(")) {
                description = description.substring(0, description.length() - 1);
            }
            briefDescriptionList.add(description.trim());
        }
    }

    private List<MetaData> parsePage(String pageSource, ProxyWebDriverWrapper webDriver) {
        List<MetaData> metaDataList = new ArrayList<>();
        final Document document = Jsoup.parse(pageSource);
        Elements caseParagraphElementList = document.select(PARAGRAPH_SELECTOR);
        List<WebElement> caseParagraphWebDriverElementList = webDriver
                .findElements(By.cssSelector(PARAGRAPH_SELECTOR_WITHOUT_LINK));
        if (caseParagraphWebDriverElementList.size() != caseParagraphElementList.size()) {
            prepareWebElementList(caseParagraphWebDriverElementList);
        }
        for (int i = 0; i < caseParagraphElementList.size(); i++) {
            Element webElement = caseParagraphElementList.get(i);
            Elements paragraphUrlList = webElement.select(LINK_SELECTOR);
            WebElement paragraphWebElement = caseParagraphWebDriverElementList.get(i);
            if (!paragraphUrlList.isEmpty()) {
                metaDataList.addAll(parseMetaDataParagraph(webElement, paragraphUrlList, paragraphWebElement));
            }
        }
        return metaDataList;
    }

    private void prepareWebElementList(List<WebElement> caseParagraphWebDriverElementList) {
        List<WebElement> listToRemove = new ArrayList<>();
        for (WebElement element : caseParagraphWebDriverElementList) {
            try {
                element.findElement(By.cssSelector(LINK_SELECTOR));
            } catch (Exception ex) {
                listToRemove.add(element);
            }
        }
        caseParagraphWebDriverElementList.removeAll(listToRemove);
    }

    private String getCaseName(Element caseParagraphElement, String caseNumber) {
        String caseName = "";
        final String dirtyCaseName = caseParagraphElement.text();
        if (StringUtils.isNotBlank(dirtyCaseName) || StringUtils.isNotEmpty(dirtyCaseName)
                || StringUtils.isNotBlank(caseNumber) || StringUtils.isNotEmpty(caseNumber)) {
            try {
                caseName = StringUtils.substringBetween(dirtyCaseName, caseNumber, DASH_WITH_TWO_SPACES).trim();
            } catch (NullPointerException ex) {
                if (dirtyCaseName.contains(CASE_NAME_PART)) {
                    caseName = StringUtils.substringBetween(dirtyCaseName, StringUtils.SPACE, CASE_NAME_PART).trim();
                } else if (dirtyCaseName.contains(DATE_SEPARATOR_WITH_BRACKET))
                    caseName = StringUtils.substringBefore(dirtyCaseName, DATE_SEPARATOR).trim();
            }
        }
        if (caseName.contains(DATE_SEPARATOR_WITH_BRACKET))
            caseName = StringUtils.substringBefore(caseName, DATE_SEPARATOR_WITH_BRACKET).trim();
        if (StringUtils.isBlank(caseName)) {
            caseName = StringUtils.substringBetween(dirtyCaseName, caseNumber.concat(" -"), "-");
        }
        if (StringUtils.isBlank(caseName)) {
            caseName = StringUtils.substringAfter(dirtyCaseName, caseNumber).trim();
        }
        return caseName;
    }

    private String getCaseNumber(Element caseParagraphElement) {
        String caseNumber = "";
        final String dirtyCaseInfo = caseParagraphElement.text();
        if (StringUtils.isNotBlank(dirtyCaseInfo) || StringUtils.isNotEmpty(dirtyCaseInfo)) {
            Pattern pattern = Pattern.compile(CASE_NUMBER_REGEX);
            Matcher matcher = pattern.matcher(dirtyCaseInfo);
            if (matcher.find()) {
                caseNumber = matcher.group();
            }
        }
        return caseNumber;
    }

}
