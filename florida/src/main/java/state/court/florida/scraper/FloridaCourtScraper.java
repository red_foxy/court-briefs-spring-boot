package state.court.florida.scraper;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import state.court.driver.ProxyWebDriverWrapper;
import state.court.entity.FileNames;
import state.court.entity.ScraperInfo;
import state.court.florida.parser.FloridaCourtParser;
import state.court.io.FileOutputImpl;
import state.court.model.MetaData;
import state.court.repository.FileNameRepository;
import state.court.repository.ScraperRepository;
import state.court.utils.ParserUtils;
import state.court.utils.ZipUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static state.court.constant.Constants.*;
import static state.court.florida.constant.FloridaCourtConstants.*;

/**
 * @author foxy
 * @since 04.12.18.
 */
@Slf4j
@Component
public class FloridaCourtScraper {

    private final ScraperRepository scraperRepository;
    private final FileNameRepository fileNameRepository;
    private final FloridaCourtParser floridaCourtParser;

    @Autowired
    public FloridaCourtScraper(ScraperRepository scraperRepository,
                               FileNameRepository fileNameRepository,
                               FloridaCourtParser floridaCourtParser) {
        this.scraperRepository = scraperRepository;
        this.fileNameRepository = fileNameRepository;
        this.floridaCourtParser = floridaCourtParser;
    }

    public void parsePage() throws IOException {
        ScraperInfo scraperInfo = scraperRepository.save(ParserUtils.saveScraperInfo(SUB_DIR_NAME));
        try (ProxyWebDriverWrapper webDriver = new ProxyWebDriverWrapper()) {
            webDriver.get(MAIN_URL);
            collectUrlAndScrape(webDriver, scraperInfo);
            log.info("Start Florida scraper");
        }
        ZipUtils.archiveFiles(SUB_DIR_NAME, SUB_DIR_NAME_UPDATE);
    }

    private void collectUrlAndScrape(ProxyWebDriverWrapper webDriver, ScraperInfo scraperInfo) throws IOException {
        final List<String> yearUrlList = new ArrayList<>();
        getUrlList(yearUrlList, webDriver);
        yearUrlList.remove(yearUrlList.size() - 1);
        parsePageByUrl(yearUrlList, scraperInfo);
    }

    private void getUrlList(List<String> yearUrlList, ProxyWebDriverWrapper webDriver) {
        scrapeBriefLinkList(yearUrlList, webDriver);
    }

    private void parsePageByUrl(List<String> yearUrlList, ScraperInfo scraperInfo) throws IOException {
        final List<String> yearSecondLevelUrlList = new ArrayList<>();
        try (final ProxyWebDriverWrapper webDriver = new ProxyWebDriverWrapper()) {
            for (String yearUrl : yearUrlList) {
                webDriver.get(yearUrl);
                getUrlList(yearSecondLevelUrlList, webDriver);
            }
            final List<MetaData> metaDataList = floridaCourtParser.parseCaseNumberPage(yearSecondLevelUrlList);
            List<String> fileNames = new ArrayList<>();
            for (MetaData metaData : metaDataList) {
                fileNames.addAll(downloadPDF(metaData, scraperInfo));
            }
            ParserUtils.setFileNameToScraperInfo(fileNames, scraperInfo);
            scraperRepository.save(scraperInfo);
        }
    }

    private List<String> downloadPDF(MetaData metaData, ScraperInfo scraperInfo) throws IOException {
        FileOutputImpl output = new FileOutputImpl(SUB_DIR_NAME);
        List<String> existedFiles = getExistFiles(scraperInfo, output);
        return output.downloadPDFWithProxy(metaData, SUB_DIR_NAME, existedFiles, scraperInfo, PROXY_HOST, PROXY_PORT);
    }

    private List<String> getExistFiles(ScraperInfo scraperInfo, FileOutputImpl output) {
        List<FileNames> fileNames = fileNameRepository.findAllByScraperInfoScraperName(scraperInfo.getScraperName());
        return output.getExistFiles(fileNames);
    }

    private void scrapeBriefLinkList(List<String> briefUrlList, ProxyWebDriverWrapper webDriver) {
        final List<WebElement> briefElementList = webDriver.findElements(By.cssSelector(URL_SELECTOR));
        for (WebElement elementUrl : briefElementList) {
            final String url = elementUrl.getAttribute(LINK_ATTR_SELECTOR);
            log.info("{}", url);
            briefUrlList.add(url);
        }
    }
}
