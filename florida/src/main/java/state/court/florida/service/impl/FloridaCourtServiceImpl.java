package state.court.florida.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import state.court.service.CourtService;
import state.court.florida.scraper.FloridaCourtScraper;

import javax.transaction.Transactional;
import java.io.IOException;

/**
 * @author foxy
 * @since 04.12.18.
 */
@Service
public class FloridaCourtServiceImpl implements CourtService {

    private final FloridaCourtScraper floridaScraper;

    @Autowired
    public FloridaCourtServiceImpl(FloridaCourtScraper floridaScraper) {
        this.floridaScraper = floridaScraper;
    }

    @Override
    @Transactional
    public void scrapeCourt() throws IOException {
        floridaScraper.parsePage();
    }
}
