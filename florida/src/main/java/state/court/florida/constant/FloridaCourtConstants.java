package state.court.florida.constant;

/**
 * @author foxy
 * @since 18.03.19.
 */
public class FloridaCourtConstants {

    public static final String MAIN_URL = "http://www.floridasupremecourt.org/clerk/briefs/index.shtml";
    public static final String URL_SELECTOR = "#main-content-container > div > div > div > ul > li > a";
    public static final String SUB_DIR_NAME = "florida";
    public static final String SUB_DIR_NAME_UPDATE = "/data/" + SUB_DIR_NAME;
    public static final String PARAGRAPH_SELECTOR = "#main-content-container > div > div > div > p:has(a[href])";
    public static final String CASE_NAME_END_STR = " -";
    public static final String FLORIDA_JURISDICTION = "Florida Supreme Court";
    public static final String DATE_REGULAR_EXPRESSION_PATTERN = "\\d{1,2}\\/\\d{1,2}\\/\\d{2,4}";
    public static final String DATE_SEPARATOR = "filed";
    public static final String DATE_SEPARATOR_WITH_BRACKET = "(filed";
    public static final String PARAGRAPH_SELECTOR_WITHOUT_LINK = "#main-content-container > div > div > div > p";
    public static final String BAD_URL_PART = ".pdf#";
    public static final String SHARP = "#";
    public static final String DASH_WITH_TWO_SPACES = " - ";
    public static final String REGEX_CLEAR_DESCRIPTION = "-";
    public static final String LARGE_DASH_CHAR = "–";
    public static final String DESCRIPTION_PART_BRIEF = "Brief";
    public static final String DESCRIPTION_PART_LIST = "List";
    public static final String CASE_NAME_PART = "Jurisdic";
    public static final String CASE_NUMBER_REGEX = "[A-Z]{2}\\d{2}-\\d{1,5}";

    private FloridaCourtConstants() {
        throw new IllegalStateException("Utility class");
    }
}
