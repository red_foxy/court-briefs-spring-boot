package state.court;

import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple FloridaStateCourtApplication.
 */
public class FloridaStateCourtApplicationTest {

    private static final String DATE_TEXT = "01/05/2015";
    private static final String EXPECTED_DATE = "2015-01-05";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String DATE_PATTERN_ARRAY = "[MM/dd/yyyy][MM/dd/yy][M/dd/yy][M/dd/yyyy]";

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void date() {
        final LocalDate parse = LocalDate.parse(DATE_TEXT, DateTimeFormatter.ofPattern(DATE_PATTERN_ARRAY));
        assertEquals("Test if failed", EXPECTED_DATE, parse.format(DateTimeFormatter.ofPattern(DATE_FORMAT)));
    }
}
