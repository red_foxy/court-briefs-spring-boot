package state.court.newhampshire.scraper;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import state.court.driver.WebDriverWrapper;
import state.court.entity.FileNames;
import state.court.entity.ScraperInfo;
import state.court.io.FileOutputImpl;
import state.court.model.MetaData;
import state.court.newhampshire.parser.NewHampshireCourtParser;
import state.court.repository.FileNameRepository;
import state.court.repository.ScraperRepository;
import state.court.utils.ParserUtils;
import state.court.utils.ZipUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static state.court.constant.Constants.LINK_ATTR_SELECTOR;
import static state.court.newhampshire.constant.NewHampshireConstants.*;

/**
 * @author foxy
 * @since 26.04.19.
 */
@Slf4j
@Component
public class NewHampshireCourtScraper {

    private final ScraperRepository scraperRepository;
    private final FileNameRepository fileNameRepository;
    private final NewHampshireCourtParser newHampshireParser;

    @Autowired
    public NewHampshireCourtScraper(ScraperRepository scraperRepository,
                                    FileNameRepository fileNameRepository,
                                    NewHampshireCourtParser newHampshireParser) {
        this.scraperRepository = scraperRepository;
        this.fileNameRepository = fileNameRepository;
        this.newHampshireParser = newHampshireParser;
    }

    public void scrapePage() throws IOException {
        ScraperInfo scraperInfo = scraperRepository.save(ParserUtils.saveScraperInfo(SUB_DIR_NAME));
        String currentYear = ParserUtils.getCurrentYear();
        String previousYear = ParserUtils.getPreviousYear();
        final List<String> briefUrlList = new ArrayList<>();
        try (final WebDriverWrapper webDriver = new WebDriverWrapper()) {
            webDriver.get(MAIN_URL);
            List<WebElement> briefUrlElementList = webDriver.findElements(By.cssSelector(GENERAL_YEAR_URL_SELECTOR));
            for (WebElement element : briefUrlElementList) {
                String url = element.getAttribute(LINK_ATTR_SELECTOR);
                if (!getExistFiles(scraperInfo).isEmpty()) {
                    if (url.contains(currentYear) || url.contains(previousYear)) {
                        log.info(url);
                        briefUrlList.add(url);
                    }
                } else {
                    briefUrlList.add(url);
                }
            }
            parseCaseNumberPage(briefUrlList, scraperInfo, webDriver);
        }
        ZipUtils.archiveFiles(SUB_DIR_NAME, SUB_DIR_NAME_UPDATE);
    }

    private void parseCaseNumberPage(List<String> briefUrlList, ScraperInfo scraperInfo, WebDriverWrapper webDriver) throws IOException {
        List<String> fileNames = new ArrayList<>();
        for (String firstLevelUrl : briefUrlList) {
            webDriver.get(firstLevelUrl);
            String pageSource = webDriver.getPageSource();
            final List<MetaData> metaDataList = newHampshireParser.parsePage(pageSource);
            for (MetaData metaData : metaDataList) {
                fileNames.addAll(downloadPDF(metaData, scraperInfo));
                log.info("File names {}", fileNames.size());
            }
        }
        ParserUtils.setFileNameToScraperInfo(fileNames, scraperInfo);
        scraperRepository.save(scraperInfo);
    }

    private List<String> downloadPDF(MetaData metaData, ScraperInfo scraperInfo) throws IOException {
        FileOutputImpl output = new FileOutputImpl(SUB_DIR_NAME);
        List<String> existedFiles = getExistFiles(scraperInfo, output);
        return output.downloadPDF(metaData, SUB_DIR_NAME, existedFiles, scraperInfo);
    }

    private List<String> getExistFiles(ScraperInfo scraperInfo, FileOutputImpl output) {
        List<FileNames> fileNames = fileNameRepository.findAllByScraperInfoScraperName(scraperInfo.getScraperName());
        return output.getExistFiles(fileNames);
    }

    private List<String> getExistFiles(ScraperInfo scraperInfo) {
        FileOutputImpl output = new FileOutputImpl(SUB_DIR_NAME);
        return getExistFiles(scraperInfo, output);
    }
}
