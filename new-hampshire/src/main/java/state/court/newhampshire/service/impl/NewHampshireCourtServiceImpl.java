package state.court.newhampshire.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import state.court.newhampshire.scraper.NewHampshireCourtScraper;
import state.court.service.CourtService;

import java.io.IOException;

/**
 * @author foxy
 * @since 26.04.19.
 */
@Service
public class NewHampshireCourtServiceImpl implements CourtService {
    private final NewHampshireCourtScraper newHampshireScraper;

    @Autowired
    public NewHampshireCourtServiceImpl(NewHampshireCourtScraper newHampshireScraper) {
        this.newHampshireScraper = newHampshireScraper;
    }

    @Override
    @Transactional
    public void scrapeCourt() throws IOException {
        newHampshireScraper.scrapePage();
    }
}
