package state.court.newhampshire.parser;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import state.court.model.MetaData;
import state.court.utils.ParserUtils;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static state.court.constant.Constants.*;
import static state.court.newhampshire.constant.NewHampshireConstants.*;

/**
 * @author foxy
 * @since 26.04.19.
 */
@Slf4j
@Component
public class NewHampshireCourtParser {
    public List<MetaData> parsePage(String pageSource) {
        List<MetaData> metaDataList = new ArrayList<>();
        Document document = Jsoup.parse(pageSource);
        if (document != null) {
            metaDataList.addAll(parseCaseNumberParagraph(document));
            log.info("MetaData List {}", metaDataList.size());
        }
        return metaDataList;
    }

    private List<MetaData> parseCaseNumberParagraph(Document document) {
        String currentPageYear = parsePageCurrentYear(document);
        List<MetaData> metaDataList = new ArrayList<>();
        List<String> appellateBriefList = new ArrayList<>();
        List<String> briefUrlDirtyTextList = new ArrayList<>();

        Elements caseBriefParagraphList = document.select(CASE_BRIEF_LI_PARAGRAPH_SELECTOR);
        if (caseBriefParagraphList.isEmpty()) {
            caseBriefParagraphList = document.select(CASE_BRIEF_LI_SELECTOR);
        }
        if (caseBriefParagraphList.isEmpty()) {
            caseBriefParagraphList = document.select(CASE_BRIEF_PARAGRAPH_SELECTOR);
        }
        for (Element paragraph : caseBriefParagraphList) {
            String generalPartCaseName = parseCaseName(paragraph);
            String caseNumber = parseCaseNumber(paragraph);
            String caseJurisdiction = parseJurisdiction(document);
            for (Element urlElement : parseUrlElements(paragraph)) {
                String urlPart = urlElement.attr(LINK_ATTR_SELECTOR);
                if (urlPart.contains(URL_PART)) {
                    appellateBriefList.add(MAIN_URL_PART + urlPart);
                    briefUrlDirtyTextList.add(urlElement.text());
                } else {
                    appellateBriefList.add(MAIN_URL_FULL_PART + currentPageYear + File.separator + urlPart);
                    briefUrlDirtyTextList.add(urlElement.text());
                }
            }
            List<String> secondPartCaseNameList = parseSecondPartCaseName(briefUrlDirtyTextList);
            List<String> briefFieldList = parseBriefFieldList(briefUrlDirtyTextList);
            for (int i = 0; i < appellateBriefList.size(); i++) {
                MetaData metaData = new MetaData();
                String caseName = generalPartCaseName + StringUtils.SPACE + secondPartCaseNameList.get(i);
                metaData.setFileName(ParserUtils.getFilename(caseNumber, caseName, 0));
                metaData.setCaseName(caseName);
                metaData.setJurisdiction(caseJurisdiction);
                metaData.setBriefField(briefFieldList.get(i));
                metaData.setDocketOrCaseNumber(caseNumber);
                metaData.setRetrieved(LocalDate.now().format(DateTimeFormatter.ofPattern(DATE_PATTERN)));
                metaData.setAppellateBriefUrl(appellateBriefList.get(i));
                metaDataList.add(metaData);
            }
            appellateBriefList.clear();
            briefUrlDirtyTextList.clear();
            secondPartCaseNameList.clear();
            briefFieldList.clear();
        }
        return metaDataList;
    }

    private String parseCaseName(Element paragraph) {
        String dirtyCaseName = paragraph.ownText();
        return StringUtils.substringAfter(dirtyCaseName, COMMA).trim();
    }

    private String parseCaseNumber(Element paragraph) {
        String dirtyCaseName = paragraph.ownText();
        return StringUtils.substringBefore(dirtyCaseName, COMMA).trim();
    }

    private String parseJurisdiction(Document document) {
        String dirtyJurisdiction = document.select(COURT_HEADER_SELECTOR).text();
        return StringUtils.substringBefore(dirtyJurisdiction, "-").trim();
    }

    private Elements parseUrlElements(Element paragraph) {
        return paragraph.select(LINK);
    }

    private List<String> parseSecondPartCaseName(List<String> briefUrlDirtyTextList) {
        List<String> briefSecondPartCaseNameList = new ArrayList<>();
        for (String dirtyCaseText : briefUrlDirtyTextList) {
            String secondPartCaseName = StringUtils.substringAfter(dirtyCaseText, COMMA);
            briefSecondPartCaseNameList.add(secondPartCaseName);
        }
        return briefSecondPartCaseNameList;
    }

    private List<String> parseBriefFieldList(List<String> briefUrlDirtyTextList) {
        List<String> briefFieldList = new ArrayList<>();
        for (String dirtyCaseText : briefUrlDirtyTextList) {
            Pattern pattern = Pattern.compile(BRIEF_FIELD_PATTERN);
            briefFieldList.add(ParserUtils.formatBriefField(ParserUtils.getString(dirtyCaseText, pattern)));
        }
        return briefFieldList;
    }

    private String parsePageCurrentYear(Document document) {
        String dirtyPageCurrentYear = document.select(COURT_HEADER_SELECTOR).text();
        Pattern pattern = Pattern.compile(CURRENT_YEAR_PATTERN);
        return ParserUtils.getString(dirtyPageCurrentYear, pattern).trim();
    }
}
