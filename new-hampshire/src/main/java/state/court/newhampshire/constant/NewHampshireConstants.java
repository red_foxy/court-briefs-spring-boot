package state.court.newhampshire.constant;

/**
 * @author foxy
 * @since 26.04.19.
 */
public class NewHampshireConstants {

    public static final String SUB_DIR_NAME = "new-hampshire";
    public static final String SUB_DIR_NAME_UPDATE = "/data/" + SUB_DIR_NAME;
    public static final String MAIN_URL = "https://www.courts.state.nh.us/supreme/ebriefs/index.htm";
    public static final String MAIN_URL_PART = "https://www.courts.state.nh.us";
    public static final String MAIN_URL_FULL_PART = "https://www.courts.state.nh.us/supreme/ebriefs/";
    public static final String BRIEF_FIELD_PATTERN = "\\d*/\\d*/\\d{2}";
    public static final String CURRENT_YEAR_PATTERN = "\\d{4}";
    public static final String GENERAL_YEAR_URL_SELECTOR = "#content > div > p > a[href*=\"htm\"]";
    public static final String CASE_BRIEF_LI_PARAGRAPH_SELECTOR = "#ebrieflist > li > p:has(a)";
    public static final String CASE_BRIEF_LI_SELECTOR = "#ebrieflist > li:has(a)";
    public static final String CASE_BRIEF_PARAGRAPH_SELECTOR = "#content > div > p:has(a)";
    public static final String URL_PART = "supreme/ebriefs";
    public static final String COURT_HEADER_SELECTOR = "#courtheadertext";
    public static final String COMMA = ",";

    private NewHampshireConstants() {
    }
}
