package state.court;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import state.court.service.CourtService;

import javax.transaction.Transactional;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class NewHampshireSateCourtApplication implements CommandLineRunner
{
    private final CourtService service;

    @Autowired
    public NewHampshireSateCourtApplication(@Qualifier("newHampshireCourtServiceImpl") CourtService service) {
        this.service = service;
    }

    public static void main( String[] args )
    {
        SpringApplication.run(NewHampshireSateCourtApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        service.scrapeCourt();
    }
}
